package me.epicmiro.pvt.settings;

public class BlockData {
	private String world;
	private int previd;
	private int newid;
	private byte prevdata;
	private byte newdata;
	private int x;
	private int y;
	private int z;
	private int gameid;

	public BlockData(int gameid, String world, int previd, byte prevdata,
			int newid, byte newdata, int x, int y, int z) {
		this.gameid = gameid;
		this.world = world;
		this.previd = previd;
		this.prevdata = prevdata;
		this.newid = newid;
		this.newdata = newdata;
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public int getGameId() {
		return this.gameid;
	}

	public String getWorld() {
		return this.world;
	}

	public byte getPrevdata() {
		return this.prevdata;
	}

	public byte getNewdata() {
		return this.newdata;
	}

	public int getPrevid() {
		return this.previd;
	}

	public int getNewid() {
		return this.newid;
	}

	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}

	public int getZ() {
		return this.z;
	}
}
