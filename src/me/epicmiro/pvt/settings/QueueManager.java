package me.epicmiro.pvt.settings;

import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import me.epicmiro.pvt.game.Game;
import me.epicmiro.pvt.game.GameManager;
import me.epicmiro.pvt.game.GameReset;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.plugin.Plugin;

public class QueueManager {
	private static QueueManager instance = new QueueManager();
	private DatabaseDumper dumper = new DatabaseDumper();
	private ArrayList<BlockData> queue = new ArrayList<BlockData>();
	private Plugin p;
	private Logger log;
	private DatabaseManager dbman = DatabaseManager.getInstance();
	private boolean sqlmode = true;

	public static QueueManager getInstance() {
		return instance;
	}

	public void setup(Plugin p, boolean sqlmode) throws SQLException {
		this.sqlmode = sqlmode;
		this.p = p;

		if (sqlmode) {
			PreparedStatement s = this.dbman
					.createStatement(" CREATE TABLE "
							+ SettingsManager.getSqlPrefix()
							+ "blocks" + SettingsManager.getSqlSuffix() +"(gameid int, world varchar(255),previd int,prevdata int,newid int, newdata int, x int, y int, z int, time long)");
			DatabaseMetaData dbm = this.dbman.getMysqlConnection()
					.getMetaData();
			ResultSet tables = dbm.getTables(null, null,
					SettingsManager.getSqlPrefix() + "blocks" + SettingsManager.getSqlSuffix(), null);
			if (!tables.next()) {
				s.execute();
			}

			this.log = p.getLogger();
			this.log.info("Connected to database.");
		}
	}

	public void rollback(GameReset r) {
		GameManager.getInstance().getGame().setRBStatus("starting rollback");

		new preRollback(r).start();
	}

	public void add(BlockData data) {
		this.queue.add(data);
		if ((!this.dumper.isAlive()) && (this.sqlmode)) {
			this.dumper = new DatabaseDumper();
			this.dumper.start();
		}
	}

	class DatabaseDumper extends Thread {
		PreparedStatement s;

		DatabaseDumper() {
		}

		public void run() {
			this.s = QueueManager.this.dbman.createStatement("INSERT INTO "
					+ SettingsManager.getSqlPrefix()
					+ "blocks" + SettingsManager.getSqlSuffix() +  " VALUES (?,?,?,?,?,?,?,?,?,?)");
			while (QueueManager.this.queue.size() > 0) {
				BlockData b = QueueManager.this.queue.remove(0);
				try {
					this.s.setInt(
							1,
							GameManager.getInstance().getBlockGameId(
									new Location(Bukkit.getWorld(b.getWorld()),
											b.getX(), b.getY(), b.getZ())));
					this.s.setString(2, b.getWorld());
					this.s.setInt(3, b.getPrevid());
					this.s.setByte(4, b.getPrevdata());
					this.s.setInt(5, b.getNewid());
					this.s.setByte(6, b.getNewdata());
					this.s.setInt(7, b.getX());
					this.s.setInt(8, b.getY());
					this.s.setInt(9, b.getZ());
					this.s.setLong(10, new Date().getTime());
					this.s.execute();
				} catch (Exception e) {
					QueueManager.this.queue.add(b);
					try {
						QueueManager.this.dbman.getMysqlConnection().close();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
					QueueManager.this.dbman.connect();
				}
			}
		}
	}

	class Rollback extends Thread {
		Statement s;
		ResultSet result;
		GameReset r;
		Game game;
		int taskID = 0;

		int rbblocks = 0;
		int total = 0;
		int run = 0;

		private Rollback(ResultSet rs, GameReset r) {
			this.result = rs;
			this.r = r;
			this.game = GameManager.getInstance().getGame();
		}

		public void setTaskId(int t) {
			this.taskID = t;
		}

		public void run() {
			if (QueueManager.this.sqlmode) {
				try {
					if (this.run == 0) {
						this.result.last();
						this.total = this.result.getRow();
						this.result.beforeFirst();
						this.run += 1;
					}

					int i = 1;
					boolean done = false;
					try {
						while ((i != 100) && (!done)) {
							this.game.setRBStatus("rollback 1");

							if (!this.result.next())
								break;
							Location l = new Location(QueueManager.this.p
									.getServer().getWorld(
											this.result.getString(2)),
									this.result.getInt(7),
									this.result.getInt(8),
									this.result.getInt(9));
							Block b = l.getBlock();
							b.setTypeId(this.result.getInt(3));
							b.setData(this.result.getByte(4));
							b.getState().update();
							i++;
							this.rbblocks += 1;
						}
						this.game.setRBPercent((0.0D + this.rbblocks)
								/ (0.0D + this.total) * 100.0D);
						if (i == 100)
							return;
						this.game.setRBStatus("finish rollback");

						done = true;
						Bukkit.getScheduler().cancelTask(this.taskID);
						this.game.setRBStatus("rollback stoppped");

						this.r.rollbackFinishedCallback();
						this.game.setRBStatus("clearing table");

						Statement s1 = QueueManager.this.dbman
								.createStatement();
						s1.execute("DELETE FROM "
								+ SettingsManager.getSqlPrefix()
								+ "blocks" + SettingsManager.getSqlSuffix());
						System.out.println("Arena reset. Rolled back " + this.rbblocks
								+ " blocks");
						this.game.setRBStatus("");

						this.result.close();
						this.result = null;
					} catch (Exception e) {
						e.printStackTrace();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				int a = QueueManager.this.queue.size() - 1;
				int rbblocks = 0;
				while (a >= 0) {
					BlockData result = QueueManager.this.queue
							.get(a);
					if (result.getGameId() == this.game.getID()) {
						QueueManager.this.queue.remove(a);
						Location l = new Location(Bukkit.getWorld(result
								.getWorld()), result.getX(), result.getY(),
								result.getZ());
						Block b = l.getBlock();
						b.setTypeId(result.getPrevid());
						b.setData(result.getPrevdata());
						b.getState().update();
						rbblocks++;
					}

					a--;
				}
				System.out.println("Arena reset. Rolled back "
						+ rbblocks + " blocks. Save containts "
						+ QueueManager.this.queue.size() + " blocks");

				Bukkit.getScheduler().cancelTask(this.taskID);
				this.r.rollbackFinishedCallback();
			}
		}
	}

	class preRollback extends Thread {
		Statement s;
		ResultSet result;
		GameReset r;
		Game game;
		int taskId = 0;

		private preRollback(GameReset r) {
			this.r = r;
			boolean done = false;
			this.game = GameManager.getInstance().getGame();
		}

		public void run() {
			this.game.setRBStatus("save queue");

			while ((QueueManager.this.queue.size() > 10)
					&& (QueueManager.this.sqlmode)) {
				this.game.setRBPercent(QueueManager.this.queue.size());
				try {
					sleep(10L);
				} catch (Exception localException1) {
				}
			}
			try {
				this.game.setRBStatus("querying");
				if (QueueManager.this.sqlmode) {
					String query = "SELECT * FROM "
							+ SettingsManager.getSqlPrefix()
							+ "blocks" + SettingsManager.getSqlSuffix()
							+ " ORDER BY time DESC";
					Statement s = QueueManager.this.dbman.createStatement();
					this.game.setRBStatus("query result");

					this.result = s.executeQuery(query);
					this.game.setRBStatus("clearing entities");
				}
				try {
					List<?> list = SettingsManager.getGameWorld()
							.getEntities();
					for (Iterator<?> entities = list.iterator(); entities
							.hasNext();)
						if (entities.hasNext()) {
							Entity entity = (Entity) entities.next();
							if ((entity instanceof Item)) {
								Item iteme = (Item) entity;
								Location loce = entity.getLocation();
								if (GameManager.getInstance().getBlockGameId(
										loce) == 1)
									iteme.remove();
							}
						}
				} catch (Exception localException2) {
				}
				this.game.setRBStatus("starting rollback");
				QueueManager.Rollback rb = new QueueManager.Rollback(
						this.result, this.r);
				int taskid = Bukkit
						.getServer()
						.getScheduler()
						.scheduleSyncRepeatingTask(QueueManager.this.p, rb, 0L,
								1L);
				rb.setTaskId(taskid);
			} catch (Exception e) {
				e.printStackTrace();
				QueueManager.this.dbman.connect();
			}
		}
	}
}
