package me.epicmiro.pvt.settings;

import java.util.HashMap;

import me.epicmiro.pvt.game.GameManager;
import me.epicmiro.pvt.game.Game;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockFadeEvent;
import org.bukkit.event.block.BlockGrowEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.material.TrapDoor;

public class LoggingManager implements Listener {
	public static HashMap<String, Integer> i = new HashMap<String, Integer>();

	private static LoggingManager instance = new LoggingManager();

	private LoggingManager() {
		i.put("BCHANGE", Integer.valueOf(1));
		i.put("BPLACE", Integer.valueOf(1));
		i.put("BFADE", Integer.valueOf(1));
		i.put("BBLOW", Integer.valueOf(1));
		i.put("BSTARTFIRE", Integer.valueOf(1));
		i.put("BBURN", Integer.valueOf(1));
		i.put("BREDSTONE", Integer.valueOf(1));
		i.put("LDECAY", Integer.valueOf(1));
		i.put("BINTERACT", Integer.valueOf(1));
	}

	public static LoggingManager getInstance() {
		return instance;
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void blockChanged(BlockBreakEvent e) {
		if (e.isCancelled())
			return;
		if (e.getPlayer().hasPermission("pvt.build.anywhere")
				&& GameManager.getInstance().getPlayerGameId(e.getPlayer()) == -1) {
			return;
		}
		logBlockDestoryed(e.getBlock());
		i.put("BCHANGE", Integer.valueOf(i.get("BCHANGE").intValue() + 1));
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void blockChanged(BlockPlaceEvent e) {
		if (e.isCancelled())
			return;
		if (e.getPlayer().hasPermission("pvt.build.anywhere")
				&& GameManager.getInstance().getPlayerGameId(e.getPlayer()) == -1) {
			return;
		}
		logBlockCreated(e.getBlock());
		i.put("BPLACE", Integer.valueOf(i.get("BPLACE").intValue() + 1));
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void blockChanged(BlockFadeEvent e) {
		if (e.isCancelled())
			return;
		logBlockDestoryed(e.getBlock());
		i.put("BFADE", Integer.valueOf(i.get("BFADE").intValue() + 1));
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void blockChange(EntityExplodeEvent e) {
		if (e.isCancelled())
			return;

		for (Block b : e.blockList()) {
			logBlockDestoryed(b);
		}

		i.put("BBLOW", Integer.valueOf(i.get("BBLOW").intValue() + 1));
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void blockChange(BlockIgniteEvent e) {
		if (e.isCancelled())
			return;

		logBlockCreated(e.getBlock());
		i.put("BSTARTFIRE", Integer.valueOf(i.get("BSTARTFIRE").intValue() + 1));
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void blockChanged(BlockBurnEvent e) {
		if (e.isCancelled())
			return;

		logBlockDestoryed(e.getBlock());
		i.put("BBURN", Integer.valueOf(i.get("BBURN").intValue() + 1));
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void blockChanged(BlockGrowEvent e) {
		if (e.isCancelled())
			return;

		logBlockCreated(e.getBlock());
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void blockChanged(LeavesDecayEvent e) {
		if (e.isCancelled())
			return;

		logBlockDestoryed(e.getBlock());
		i.put("LDECAY", Integer.valueOf(i.get("LDECAY").intValue() + 1));
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void blockChange(PlayerInteractEvent e) {
		if (e.isCancelled())
			return;
		
		if ((e.getAction() == Action.PHYSICAL) && (e.getClickedBlock().getType() == Material.SOIL))
		      e.setCancelled(true);
		
		if (e.getPlayer().hasPermission("pvt.build.anywhere")
				&& GameManager.getInstance().getPlayerGameId(e.getPlayer()) == -1) {
			return;
		}
		
		if(e.getClickedBlock() == null){
			e.setCancelled(true);
			return;
		}
		
		if(e.getClickedBlock().getType() == Material.TRAP_DOOR || e.getClickedBlock().getType() == Material.WOODEN_DOOR){
		e.setCancelled(true);
		if (isTopDoor(e.getClickedBlock())) {
			Block bl = e
					.getClickedBlock()
					.getWorld()
					.getBlockAt(e.getClickedBlock().getX(),
							e.getClickedBlock().getY() - 1,
							e.getClickedBlock().getZ());
			logBlockChanged(bl);
		} else {
			logBlockChanged(e.getClickedBlock());
		}

		i.put("BINTERACT", Integer.valueOf(i.get("BINTERACT").intValue() + 1));
		}
	}

	public void logBlockCreated(Block b) {
		if (GameManager.getInstance().getBlockGameId(b.getLocation()) == -1)
			return;
		if (GameManager.getInstance().getGameMode(
				GameManager.getInstance().getBlockGameId(b.getLocation())) == Game.GameMode.DISABLED)
			return;
		QueueManager.getInstance().add(
				new BlockData(GameManager.getInstance().getBlockGameId(
						b.getLocation()), b.getWorld().getName(), 0, (byte) 0,
						b.getTypeId(), b.getData(), b.getX(), b.getY(), b
								.getZ()));
	}

	public void logBlockDestoryed(Block b) {
		if (GameManager.getInstance().getBlockGameId(b.getLocation()) == -1)
			return;
		if (GameManager.getInstance().getGameMode(
				GameManager.getInstance().getBlockGameId(b.getLocation())) == Game.GameMode.DISABLED)
			return;
		if (b.getTypeId() == 51)
			return;
		QueueManager.getInstance().add(
				new BlockData(GameManager.getInstance().getBlockGameId(
						b.getLocation()), b.getWorld().getName(),
						b.getTypeId(), b.getData(), 0, (byte) 0, b.getX(), b
								.getY(), b.getZ()));
	}

	static boolean isDoorClosed(Block block) {
		if (block.getType() == Material.TRAP_DOOR) {
			TrapDoor trapdoor = (TrapDoor) block.getState().getData();
			return !trapdoor.isOpen();
		} else {
			byte data = block.getData();
			if ((data & 0x8) == 0x8) {
				block = block.getRelative(BlockFace.DOWN);
				data = block.getData();
			}
			return ((data & 0x4) == 0);
		}
	}

	static byte openDoor(Block block) {
		byte datareturn;
		if (block.getType() == Material.TRAP_DOOR) {
			BlockState state = block.getState();
			TrapDoor trapdoor = (TrapDoor) state.getData();
			trapdoor.setOpen(false);
			datareturn = trapdoor.getData();
			trapdoor.setOpen(true);
			state.update();

		} else {
			byte data = block.getData();
			if ((data & 0x8) == 0x8) {
				block = block.getRelative(BlockFace.DOWN);
				data = block.getData();
			}
			data = (byte) (data | 0x4);
			block.setData(data, true);
			datareturn = block.getData();
			block.setData(data, true);

		}
		return datareturn;
	}

	static boolean isTopDoor(Block block) {
		boolean truevalue;
		if (block.getType() == Material.TRAP_DOOR) {
			truevalue = false;

		} else {
			byte data = block.getData();
			if ((data & 0x8) == 0x8) {
				truevalue = true;
			} else {
				truevalue = false;
			}
		}
		return truevalue;
	}

	static byte closeDoor(Block block) {
		byte datareturn;
		if (block.getType() == Material.TRAP_DOOR) {
			BlockState state = block.getState();
			TrapDoor trapdoor = (TrapDoor) state.getData();
			trapdoor.setOpen(true);
			datareturn = trapdoor.getData();
			trapdoor.setOpen(false);
			state.update();
		} else {
			byte data = block.getData();
			if ((data & 0x8) == 0x8) {
				block = block.getRelative(BlockFace.DOWN);
				data = block.getData();
			}
			data = (byte) (data & 0xb);
			block.setData(data, true);
			datareturn = block.getData();
			block.setData(data, true);
		}
		return datareturn;
	}

	public void logBlockChanged(Block b) {
		if (GameManager.getInstance().getBlockGameId(b.getLocation()) == -1)
			return;
		if (GameManager.getInstance().getGameMode(
				GameManager.getInstance().getBlockGameId(b.getLocation())) == Game.GameMode.DISABLED)
			return;
		if (b.getTypeId() == 51)
			return;

		byte olddata = b.getData();
		byte newdata;
		if (isDoorClosed(b)) {
			newdata = openDoor(b);
		} else {
			newdata = closeDoor(b);
		}

		QueueManager.getInstance().add(
				new BlockData(GameManager.getInstance().getBlockGameId(
						b.getLocation()), b.getWorld().getName(),
						b.getTypeId(), olddata, b.getTypeId(), newdata, b
								.getX(), b.getY(), b.getZ()));
	}

}
