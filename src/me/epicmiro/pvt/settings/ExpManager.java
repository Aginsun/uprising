package me.epicmiro.pvt.settings;

import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Logger;

import net.arkipelego.economy.EconomyManager;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class ExpManager {

	private static ExpManager instance = new ExpManager();
	private DatabaseManager dbman = DatabaseManager.getInstance();
	private Plugin p;
	private boolean sqlmode;
	private Logger log;
	
	//PLAYER DATA (NEEDS SAVING AFTER EACH GAME)
	private HashMap<String, Integer> killsMap = new HashMap<String, Integer>();
	private HashMap<String, Integer> deathsMap = new HashMap<String, Integer>();
	private HashMap<String, Integer> playerExpMap = new HashMap<String, Integer>();
	private static HashMap<String, String> classesMap = new HashMap<String, String>();
	private HashMap<String, Integer> moneyEarned = new HashMap<String, Integer>();
	
	public static ExpManager getInstance() {
		return instance;
	}
	
	public void getStuff()
	{
		PreparedStatement s = this.dbman.createStatement(" SELECT * FROM " + SettingsManager.getSqlPrefix() + "players");
		try{
			ResultSet rs = s.executeQuery();
			while(rs.next())
			{
				String username = rs.getString("playername");
				killsMap.put(username, rs.getInt("kills"));
				deathsMap.put(username, rs.getInt("deaths"));
				playerExpMap.put(username, rs.getInt("playerexp"));
				classesMap.put(username, rs.getString("classes"));
			}
		}catch(Exception e){ e.printStackTrace();}
	}
	
	public void setup(Plugin p, boolean sqlmode) throws SQLException
	{
		this.sqlmode = sqlmode;
		this.p = p;
		
		getStuff();
		
		if (sqlmode) {
			PreparedStatement s = this.dbman.
					createStatement(" CREATE TABLE "
							+ SettingsManager.getSqlPrefix()
							+ "players(playername varchar(255), kills int, deaths int, playerexp int, classes varchar(255))");
			DatabaseMetaData dbm = this.dbman.getMysqlConnection()
					.getMetaData();
			ResultSet tables = dbm.getTables(null, null,
					SettingsManager.getSqlPrefix() + "players", null);
			if (!tables.next()) {
				s.execute();
			}

			this.log = p.getLogger();
			this.log.info("Connected to database.");
		}
	}

	public void PlayerJoin(Player p) throws SQLException 
	{
		PreparedStatement s = this.dbman.createStatement(" SELECT classes FROM " + SettingsManager.getSqlPrefix() + "players WHERE playername = '" + p.getName() + "'");
		ResultSet rs = s.executeQuery();
		
		if(rs.next())
		{
			classesMap.put(p.getName(), rs.getString("classes"));
		}
		
		if(!killsMap.containsKey(p.getName()))
			if(!deathsMap.containsKey(p.getName()))
				if(!playerExpMap.containsKey(p.getName()))
					if(!classesMap.containsKey(p.getName()))
					{
						killsMap.put(p.getName(), 0);
						deathsMap.put(p.getName(), 0);
						playerExpMap.put(p.getName(), 0);
						classesMap.put(p.getName(), "1;0;0;0;0");
						
						PreparedStatement st = this.dbman.createStatement("INSERT INTO `pvt_players` VALUES('" + p.getName() +"', 0, 0, 0, '1;0;0;0;0')");						
						try
						{
							st.execute();
						}
						catch (SQLException e)
						{
							e.printStackTrace();
						}
					}
		
	}
	
	public int GetPlayerExp(Player p) 
	{
		if(playerExpMap.containsKey(p.getName()))
			return playerExpMap.get(p.getName());
		return 0;
	}
	
	public void updateExpBar(Player p, int amount)
	{
		//this.p.getLogger().info("Updating XP BAR" + GetPlayerExp(p));
		 p.setTotalExperience(amount);
		 p.setLevel(0);
		 p.setExp(0);
		 for(;amount > p.getExpToLevel();)
		 {
			 amount -= p.getExpToLevel();
			 p.setLevel(p.getLevel()+1);
		 }
		 float xp = (float)amount / (float)p.getExpToLevel();
		 p.setExp(xp);
	}
	
	public void updateExpBar(Player p)
	{
		updateExpBar(p, GetPlayerExp(p));
	}
	
	public void addMoneyToCache(Player player, int amount) {
		if (!moneyEarned.containsKey(player.getName()))
		{
			moneyEarned.put(player.getName(), amount);
		} else {
			moneyEarned.put(player.getName(), amount + moneyEarned.get(player.getName()));
		}
	}
	
	public int GetKills(Player p)
	{
		if(killsMap.containsKey(p.getName()))
			return killsMap.get(p.getName());
		return 0;
	}
	
	public int GetDeaths(Player p) 
	{
		if(deathsMap.containsKey(p.getName()))
			return deathsMap.get(p.getName());
		return 0;
	}
	
	public static String getClasses(Player p)
	{
		if(classesMap.containsKey(p.getName()))
			return classesMap.get(p.getName());
		return null;
	}
	
	public boolean doesPlayerOwnClass(Player p, String s)
	{
		if(classesMap.containsKey(p.getName()))
		{
			String classes = getClasses(p);
			String[] classList = classes.split(";");
			if(s.equals("medic")) {
				if(classList[1].equals("1"))
					return true;
			}
			if(s.equals("sniper")) {
				if(classList[2].equals("1"))
					return true;
			}
			if(s.equals("pyro")) {
				if(classList[3].equals("1"))
					return true;
			}
			if(s.equals("bomber")) {
				if(classList[4].equals("1"))
					return true;
			}
		}
		return false;
	}
	
	public void AddPlayerExp(Player p, int exp) 
	{
		int expValue = GetPlayerExp(p);
		expValue += exp;
		SetExp(p, expValue);
	}
	
	public void SetExp(Player p, int exp)
	{
		playerExpMap.put(p.getName(), exp);
		updateExpBar(p, exp);
	}
	
	public static boolean addClasses(Player player, String className) throws SQLException
	{
		String Classes = getClasses(player);
		String[] classes = Classes.split(";");
				
		if(className.equals("medic")) {
			if(classes[1].equals("0"))
			{	classes[1] = "1"; }
		
		} else if(className.equals("sniper")) {
			if(classes[2].equals("0"))
			{	classes[2] = "1"; }
		
		} else if(className.equals("pyro")) {
			if(classes[3].equals("0")) {	
				classes[3] = "1";
			}
		} else if(className.equals("bomber")){
			if(classes[4].equals("0"))
			{	classes[4] = "1";}
		}
		
		String Class = new StringBuilder().append(classes[0]).append(";").append(classes[1]).append(";").append(classes[2]).append(";").append(classes[3]).append(";").append(classes[4]).toString();
		classesMap.put(player.getName(), Class);
			
		PreparedStatement s = DatabaseManager.getInstance().createStatement("SELECT * FROM " + SettingsManager.getSqlPrefix() + "players WHERE playername = ?");
		s.setString(1, player.getName());
		ResultSet rs = s.executeQuery();
		
		if(rs.next())
		{
			PreparedStatement st = DatabaseManager.getInstance().createStatement("UPDATE " + SettingsManager.getSqlPrefix() 
															  + "players SET classes = ? WHERE playername = ?");
			st.setString(1, classesMap.get(player.getName()));
			st.setString(2, player.getName());
			st.executeUpdate();
		}
		return true;
	}
	
	public void AddKill(Player p) 
	{
		int kills = GetKills(p);
		kills++;
		setKills(p, kills);
	}
	
	public void setKills(Player p, int kills)
	{
		killsMap.put(p.getName(), kills);
	}
	
	public void AddDeath(Player p) 
	{
		int deaths = deathsMap.get(p.getName());
		deaths++;
		setDeaths(p, deaths);
	}
	
	public void setDeaths(Player p, int deaths)
	{
		deathsMap.put(p.getName(), deaths);
	}
	
	public void updateAll() throws SQLException
	{
		
		
		if(sqlmode)
		{
			p.getLogger().info("Saving Kills map to database");
			for(String string : killsMap.keySet())
			{
				PreparedStatement s = this.dbman.createStatement("SELECT * FROM " + SettingsManager.getSqlPrefix() + "players WHERE playername = ?");
				s.setString(1, string);
				ResultSet rs = s.executeQuery();
				if(rs.next())
				{
					PreparedStatement st = this.dbman.createStatement("UPDATE " + SettingsManager.getSqlPrefix() 
																	  + "players SET kills = ? WHERE playername = ?");
					st.setInt(1, killsMap.get(string));
					st.setString(2, string);
					st.executeUpdate();
				}
			}
			p.getLogger().info("Saving Deaths map to database");
			for(String string : deathsMap.keySet())
			{
				PreparedStatement s = this.dbman.createStatement("SELECT * FROM " + SettingsManager.getSqlPrefix() + "players WHERE playername = ?");
				s.setString(1, string);
				ResultSet rs = s.executeQuery();
				if(rs.next())
				{
					PreparedStatement st = this.dbman.createStatement("UPDATE " + SettingsManager.getSqlPrefix() 
																	  + "players SET deaths = ? WHERE playername = ?");
					st.setInt(1, deathsMap.get(string));
					st.setString(2, string);
					st.executeUpdate();
				}
			}
			p.getLogger().info("Saving Experience to database");
			for(String string : playerExpMap.keySet())
			{
				PreparedStatement s = this.dbman.createStatement("SELECT * FROM " + SettingsManager.getSqlPrefix() + "players WHERE playername = ?");
				s.setString(1, string);
				ResultSet rs = s.executeQuery();
				if(rs.next())
				{
					PreparedStatement st = this.dbman.createStatement("UPDATE " + SettingsManager.getSqlPrefix() 
																	  + "players SET playerexp = ? WHERE playername = ?");
					st.setInt(1, playerExpMap.get(string));
					st.setString(2, string);
					st.executeUpdate();
				}
			}
			/*
			p.getLogger().info("Saving Classes to database");
			for(String string : classesMap.keySet())
			{
				PreparedStatement s = this.dbman.createStatement("SELECT * FROM " + SettingsManager.getSqlPrefix() + "players WHERE playername = ?");
				s.setString(1, string);
				ResultSet rs = s.executeQuery();
				if(rs.next())
				{
					PreparedStatement st = this.dbman.createStatement("UPDATE " + SettingsManager.getSqlPrefix() 
																	  + "players SET classes = ? WHERE playername = ?");
					st.setString(1, classesMap.get(string));
					st.setString(2, string);
					st.executeUpdate();
				}
			}
			*/
			p.getLogger().info("Adding Money earned to database");
			for (String playerName: moneyEarned.keySet())
			{
				EconomyManager.instance().increaseBalance(p.getServer().getOfflinePlayer(playerName), moneyEarned.get(playerName));
			}
			moneyEarned.clear();
			moneyEarned = new HashMap<String, Integer>();
		}
	}
}
