package me.epicmiro.pvt.settings;

import java.io.File;
import java.io.IOException;

import me.epicmiro.pvt.game.GameManager;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;

public class SettingsManager
{
  private static SettingsManager instance = new SettingsManager();
  private static Plugin p;
  private FileConfiguration spawns;
  private FileConfiguration system;
  private File f;
  private File f2;

  public static SettingsManager getInstance()
  {
    return instance;
  }

  public void setup(Plugin p)
  {
    SettingsManager.p = p;

    p.getConfig().options().copyDefaults(true);
    p.saveDefaultConfig();

    this.f = new File(p.getDataFolder() + File.separator + "spawns.yml");
    this.f2 = new File(p.getDataFolder() + File.separator + "system.yml");
    
    try {
      if (!this.f.exists())
        this.f.createNewFile();
      if (!this.f2.exists())
        this.f2.createNewFile(); 
    } catch (Exception localException) {  }

    reloadSystem();
    saveSystemConfig();
    reloadSystem();
    reloadSpawns();
    saveSpawns();
    reloadSpawns();

  }

  public void set(String arg0, Object arg1) {
    p.getConfig().set(arg0, arg1);
  }

  public FileConfiguration getConfig() {
    return p.getConfig();
  }

  public FileConfiguration getSystemConfig() {
    return this.system;
  }

  public FileConfiguration getSpawns() {
    return this.spawns;
  }

  public static World getGameWorld() {
    if (getInstance().getSystemConfig().getString("pvt.arena.world") == null) {
      return null;
    }
    return p.getServer().getWorld(getInstance().getSystemConfig().getString("pvt.arena.world"));
  }

  public void reloadSpawns() {
    this.spawns = YamlConfiguration.loadConfiguration(this.f);
  }

  public void reloadSystem() {
    this.system = YamlConfiguration.loadConfiguration(this.f2);
  }

  public void saveSystemConfig() {
    try {
      this.system.save(this.f2);
    }
    catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void saveSpawns() {
    try {
      this.spawns.save(this.f);
    }
    catch (IOException e) {
      e.printStackTrace();
    }
  }

  public int getSpawnCount(String group) {
    return this.spawns.getInt("spawns." + group + ".count");
  }
  
  public int getAutoStart() {
	    return getInstance().getConfig().getInt("auto-start-players");
	  }
  
  public Location getLobbySpawn()
  {
	  if(this.system.getString("pvt.lobby.spawn.world") != null){
    return new Location(Bukkit.getWorld(this.system.getString("pvt.lobby.spawn.world")), 
      this.system.getInt("pvt.lobby.spawn.x"), 
      this.system.getInt("pvt.lobby.spawn.y"), 
      this.system.getInt("pvt.lobby.spawn.z"));
	  }
	  return null;
  }

  public void setLobbySpawn(Location l) {
    this.system.set("pvt.lobby.spawn.world", l.getWorld().getName());
    this.system.set("pvt.lobby.spawn.x", Integer.valueOf(l.getBlockX()));
    this.system.set("pvt.lobby.spawn.y", Integer.valueOf(l.getBlockY()));
    this.system.set("pvt.lobby.spawn.z", Integer.valueOf(l.getBlockZ()));
  }

  public Location getSpawnPoint(int spawnid, String group) {
    return new Location(getGameWorld(), 
      this.spawns.getInt("spawns." + group + "." + spawnid + ".x"), 
      this.spawns.getInt("spawns." + group + "." + spawnid + ".y"), 
      this.spawns.getInt("spawns." + group + "." + spawnid + ".z"));
  }
  public void setSpawn(int spawnid, String group, Vector v) {
    this.spawns.set("spawns." + group + "." + spawnid + ".x", Integer.valueOf(v.getBlockX()));
    this.spawns.set("spawns." + group + "." + spawnid + ".y", Integer.valueOf(v.getBlockY()));
    this.spawns.set("spawns." + group + "." + spawnid + ".z", Integer.valueOf(v.getBlockZ()));
    if (spawnid > this.spawns.getInt("spawns." + group + ".count"))
      this.spawns.set("spawns." + group + ".count", Integer.valueOf(spawnid));
    try
    {
      this.spawns.save(this.f);
    }
    catch (IOException localIOException) {
    }
    if(group == "terrorist"){
    GameManager.getInstance().getGame().addTerroristSpawn();
    }else if (group == "police"){
    	GameManager.getInstance().getGame().addPoliceSpawn();
    }
  }

  public static String getSqlPrefix()
  {
    return getInstance().getConfig().getString("sql.prefix");
  }
  
  public static String getSqlSuffix()
  {
    return getInstance().getConfig().getString("sql.server-prefix");
  }
}