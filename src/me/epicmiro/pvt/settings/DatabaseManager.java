package me.epicmiro.pvt.settings;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

public class DatabaseManager {
	private Connection conn;
	private Plugin p;
	private Logger log;
	private static DatabaseManager instance = new DatabaseManager();

	public static DatabaseManager getInstance() {
		return instance;
	}

	public void setup(Plugin p) {
		this.log = p.getLogger();
		connect();
	}

	public Connection getMysqlConnection() {
		return this.conn;
	}

	public boolean connectToDB(String host, int port, String db, String user,
			String pass) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			this.conn = DriverManager.getConnection("jdbc:mysql://" + host
					+ ":" + port + "/" + db, user, pass);
			return true;
		} catch (ClassNotFoundException e) {
			this.log.warning("Couldn't start MySQL Driver. Stopping...\n"
					+ e.getMessage());

			return false;
		} catch (SQLException e) {
			this.log.warning("Couldn't connect to MySQL database. Stopping...\n"
					+ e.getMessage());
		}
		return false;
	}

	public PreparedStatement createStatement(String query) {
		boolean created = false;
		int times = 0;
		PreparedStatement p = null;
		try {
			times++;
			p = this.conn.prepareStatement(query, 1);
			created = true;
		} catch (SQLException e) {
			if (times == 5) {
				return null;
			}
			connect();
		}

		return p;
	}

	public Statement createStatement() {
		try {
			return this.conn.createStatement();
		} catch (SQLException e) {
		}
		return null;
	}

	public boolean connect() 
	{
		FileConfiguration c = SettingsManager.getInstance().getConfig();
		ConfigurationSection co = c.getConfigurationSection("sql");
		String host = co.getString("host", "localhost");
		int port = co.getInt("port", 3306);
		String db = co.getString("database", "PvT");
		String user = co.getString("user", "root");
		String pass = co.getString("pass", "");
		return connectToDB(host, port, db, user, pass);
	}
}