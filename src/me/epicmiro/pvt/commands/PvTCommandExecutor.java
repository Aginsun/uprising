package me.epicmiro.pvt.commands;

import java.sql.SQLException;
import java.util.HashMap;

import me.epicmiro.pvt.game.Game;
import me.epicmiro.pvt.game.GameManager;
import me.epicmiro.pvt.settings.ExpManager;
import me.epicmiro.pvt.settings.SettingsManager;
import net.arkipelego.economy.EconomyManager;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class PvTCommandExecutor implements CommandExecutor {

	Plugin p;
	HashMap<Integer, Integer> nextpolice = new HashMap<Integer, Integer>();
	HashMap<Integer, Integer> nextterrorist = new HashMap<Integer, Integer>();

	public PvTCommandExecutor(Plugin p) {
		this.p = p;
	}

	public void loadNextSpawn(String group) {
		Game g = GameManager.getInstance().getGame();
			if (group == "police") {
				this.nextpolice.put(Integer.valueOf(g.getID()), Integer
						.valueOf(SettingsManager.getInstance().getSpawnCount(
								 "police") + 1));
			} else if (group == "terrorist") {
				this.nextterrorist.put(Integer.valueOf(g.getID()), Integer
						.valueOf(SettingsManager.getInstance().getSpawnCount(
								"terrorist") + 1));
			}
	}

	@Override
	public boolean onCommand(CommandSender player, Command cmd, String label, String[] args) 
	{
		if(args.length == 0)
		{
			player.sendMessage("Plugin made by epicmiro. not for distribution without permission.");
		}
		else
		{
			if (args[0].equalsIgnoreCase("leave")) 
			{
				if(args.length == 1)
				{
					if (GameManager.getInstance().getPlayerGameId((Player) player) == -1) 
					{
					      player.sendMessage(ChatColor.RED + "Not in a game!");
					    }
					    else {
					      GameManager.getInstance().removePlayer((Player) player);
					    }
					    return true;
				}else{
					player.sendMessage("/pvt leave");
				}
					  }

			else if (args[0].equalsIgnoreCase("setarena")) {
				
					if ((!player.hasPermission("pvt.arena.create"))
							&& (!player.isOp())) {
						player.sendMessage(ChatColor.RED + "No Permission");
						return true;
					}
					if (args.length == 1) {
					GameManager.getInstance().createArenaFromSelection(
							(Player) player);
					return true;
				} else {
					player.sendMessage("/pvt createarena");
				}
			}

			else if (args[0].equalsIgnoreCase("setlobbyspawn")) {

				
					if ((!player.hasPermission("pvt.lobby.set"))
							&& (!player.isOp())) {
						player.sendMessage(ChatColor.RED + "No Permission");
						return true;
					}
					if (args.length == 1) {
					SettingsManager.getInstance().setLobbySpawn(
							((Player) player).getLocation());
					player.sendMessage(ChatColor.GREEN
							+ "Lobby spawnpoint set!");
					return true;
				} else {
					player.sendMessage("/pvt setlobbyspawn");
				}
			}
			
			else if (args[0].equalsIgnoreCase("disable")){
				
				if ((!player.hasPermission("pvt.arena.disable")) && (!player.isOp())) {
				      player.sendMessage(ChatColor.RED + "No Permission");
				      return true;
				    }

				    if (args.length == 2) {
				        GameManager.getInstance().getGame().disable();
				      player.sendMessage(ChatColor.GREEN + "Arena Disabled.");
				    }
				    else{
				    	player.sendMessage("/pvt disable");
				    }
				    return true;
				  }
			
			else if (args[0].equalsIgnoreCase("enable")){
				
				if ((!player.hasPermission("pvt.arena.enable")) && (!player.isOp())) {
				      player.sendMessage(ChatColor.RED + "No Permission");
				      return true;
				    }

				    if (args.length == 2) {
				        GameManager.getInstance().getGame().enable();
				      player.sendMessage(ChatColor.GREEN + "Arena enabled");
				    }
				    else{
				    	player.sendMessage("/pvt disable");
				    }
				    return true;
				  }
			
			else if (args[0].equalsIgnoreCase("deletearena")) {
			if ((!player.hasPermission("pvt.arena.delete")) && (!player.isOp())) {
			      player.sendMessage(ChatColor.RED + "No Permission");
			      return true;
			    }
			    
			    FileConfiguration s = SettingsManager.getInstance().getSystemConfig();

			    int arena = Integer.parseInt(args[1]);
			    Game g = GameManager.getInstance().getGame();
			    g.disable();
			    s.set("pvt.arena.enabled", Boolean.valueOf(false));

			    player.sendMessage(ChatColor.GREEN + "Arena deleted");

			    SettingsManager.getInstance().saveSystemConfig();
			    GameManager.getInstance().removeArena(arena);
			    return true;
			  }

			else if (args[0].equalsIgnoreCase("setarenaspawn")) {

				if ((!player.hasPermission("pvt.arena.setspawn"))
						&& (!player.isOp())) {
					player.sendMessage(ChatColor.RED + "No Permission");
					return true;
				}
				if (args.length == 3) {
					loadNextSpawn("police");
					loadNextSpawn("terrorist");
					System.out.println("settings spawn");
					Location l = ((Player) player).getLocation();
					int game = GameManager.getInstance().getBlockGameId(l);
					System.out.println(game);
					if (game == -1) {
						player.sendMessage(ChatColor.RED
								+ "Must be in an arena!");
						return true;
					}
					int i = 0;
					if (args[1].equalsIgnoreCase("terrorist")) {
						if (args[2].equalsIgnoreCase("next")) {
							i = ((Integer) this.nextterrorist.get(Integer
									.valueOf(game))).intValue();
							this.nextterrorist
									.put(Integer.valueOf(game),
											Integer.valueOf(((Integer) this.nextterrorist
													.get(Integer.valueOf(game)))
													.intValue() + 1));
						} else {
							try {
								i = Integer.parseInt(args[2]);
								if ((i > ((Integer) this.nextterrorist
										.get(Integer.valueOf(game))).intValue() + 1)
										|| (i < 1)) {
									player.sendMessage(ChatColor.RED
											+ "Spawn must be between 1 & "
											+ this.nextterrorist.get(Integer
													.valueOf(game)));
									return true;
								}
								if (i == ((Integer) this.nextterrorist
										.get(Integer.valueOf(game))).intValue())
									this.nextterrorist
											.put(Integer.valueOf(game),
													Integer.valueOf(((Integer) this.nextterrorist.get(Integer
															.valueOf(game)))
															.intValue() + 1));
							} catch (Exception e) {
								player.sendMessage(ChatColor.RED
										+ "Malformed input. Must be \"next\" or a number");
								return false;
							}
						}
					} else if (args[1].equalsIgnoreCase("police")) {
						if (args[2].equalsIgnoreCase("next")) {
							i = ((Integer) this.nextpolice.get(Integer
									.valueOf(game))).intValue();
							this.nextpolice.put(Integer.valueOf(game), Integer
									.valueOf(((Integer) this.nextpolice
											.get(Integer.valueOf(game)))
											.intValue() + 1));
						} else {
							try {
								i = Integer.parseInt(args[2]);
								if ((i > ((Integer) this.nextpolice.get(Integer
										.valueOf(game))).intValue() + 1)
										|| (i < 1)) {
									player.sendMessage(ChatColor.RED
											+ "Spawn must be between 1 & "
											+ this.nextpolice.get(Integer
													.valueOf(game)));
									return true;
								}
								if (i == ((Integer) this.nextpolice.get(Integer
										.valueOf(game))).intValue())
									this.nextpolice
											.put(Integer.valueOf(game),
													Integer.valueOf(((Integer) this.nextpolice.get(Integer
															.valueOf(game)))
															.intValue() + 1));
							} catch (Exception e) {
								player.sendMessage(ChatColor.RED
										+ "Malformed input. Must be \"next\" or a number");
								return false;
							}
						}
					} else {
						player.sendMessage(ChatColor.RED
								+ "use police or terrorist as the second argument.");
						return false;
					}
					if (i == -1) {
						player.sendMessage(ChatColor.RED
								+ "You must be inside an arnea");
						return true;
					}

					SettingsManager.getInstance().setSpawn(i, args[1],
							l.toVector());
					player.sendMessage(ChatColor.GREEN + "Spawn " + i
							+ " set!");

					return true;
				}
				player.sendMessage("Not enough or too many arguments");
				return false;
			}
			else if(args[0].equals("buyclass"))
			{
				if(args.length < 2)
					player.sendMessage("Add more arguments");
				else
				{
					if(args[1].equals("medic"))
					{
						boolean canAfford = false;
						try {
							canAfford = EconomyManager.instance().canAfford((OfflinePlayer) player, 800);
						} catch (SQLException e) {
							e.printStackTrace();
						}
						if(canAfford)
						{
							try{ EconomyManager.instance().decreaseBalance((OfflinePlayer) player, 800);
								ExpManager.addClasses((Player) player, "medic");
							} catch(Exception e) {}
						}
					}
					if(args[1].equals("sniper"))
					{
						boolean canAfford = false;
						try {
							canAfford = EconomyManager.instance().canAfford((OfflinePlayer) player, 800);
						} catch (SQLException e) {
							e.printStackTrace();
						}
						if(canAfford)
						{
							try
							{	
								EconomyManager.instance().decreaseBalance((OfflinePlayer) player, 800);
								ExpManager.addClasses((Player) player, "sniper");
							} catch(Exception e) {}
						}
					}
					if(args[1].equals("pyro"))
					{
						boolean canAfford = false;
						try {
							canAfford = EconomyManager.instance().canAfford((OfflinePlayer) player, 1600);
						} catch (SQLException e) {
							e.printStackTrace();
						}
						if(canAfford)
						{
							try{ EconomyManager.instance().decreaseBalance((OfflinePlayer) player, 1600);
							ExpManager.addClasses((Player) player, "pyro");
							} catch(Exception e) {}
						}
					}
					if(args[1].equals("bomber"))
					{
						boolean canAfford = false;
						try {
							canAfford = EconomyManager.instance().canAfford((OfflinePlayer) player, 1600);
						} catch (SQLException e) {
							e.printStackTrace();
						}
						if(canAfford)
						{
							try{ EconomyManager.instance().decreaseBalance((OfflinePlayer) player, 1600);
							ExpManager.addClasses((Player) player, "bomber");
							} catch(Exception e) {}
						}
					}
				}
			}
			return false;
		}
		return false;
	}
}
