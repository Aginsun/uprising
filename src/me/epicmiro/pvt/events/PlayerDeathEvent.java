package me.epicmiro.pvt.events;

import me.epicmiro.pvt.game.Game;
import me.epicmiro.pvt.game.Game.GameMode;
import me.epicmiro.pvt.game.GameManager;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.plugin.Plugin;

public class PlayerDeathEvent implements Listener {

	Plugin p;

	public PlayerDeathEvent(Plugin p) {
		this.p = p;
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerDeathByEntityEvent(EntityDamageByEntityEvent event) {
		event.setCancelled(false);

		if (!(event.getEntity() instanceof Player)) {
			return;
		}

			Player p = (Player) event.getEntity();

			int gameid = GameManager.getInstance().getPlayerGameId(p);

			if (gameid == -1) {
				event.setCancelled(true);
				return;
			}

			Entity attackerentity = event.getDamager();

			Player attacker = null;

			if (attackerentity instanceof Player) {
				attacker = (Player) attackerentity;
			}
			else if (attackerentity instanceof Projectile) {
				if(FireBallEvent.fireballs.containsKey(attackerentity.getEntityId())){
					attacker = Bukkit.getPlayer(FireBallEvent.fireballs.get(attackerentity.getEntityId()));
					FireBallEvent.fireballs.remove(attackerentity.getEntityId());
				}
				else{
				attacker = (Player)((Projectile) attackerentity).getShooter();
				}
			}else if(attackerentity instanceof TNTPrimed){
				if(TNTEvent.tntexplosions.containsKey(attackerentity.getEntityId())){
					attacker = Bukkit.getPlayer(TNTEvent.tntexplosions.get(attackerentity.getEntityId()));
					TNTEvent.tntexplosions.remove(attackerentity.getEntityId());
				}else{
				event.setCancelled(true);
				return;
				}
			}else{
				event.setCancelled(true);
				return;
			}
			
			int attgameid = -1;
			if(attacker != null){
			attgameid = GameManager.getInstance().getPlayerGameId(attacker);
			}else{
			attgameid = -1;
			}

			
			if (attacker != null && attgameid == -1) {
				event.setCancelled(true);
				return;
			}
			
			attacker.getItemInHand().setDurability((short) 0);

			Game game = GameManager.getInstance().getGame();

			if (attacker != null) {
				if (game.isPolice(p) && game.isPolice(attacker)) {
					event.setCancelled(true);
					return;
				}

				if (game.isTerrorist(p) && game.isTerrorist(attacker)) {
					event.setCancelled(true);
					return;
				}
			}

			if (game.getGameMode() == GameMode.WAITING) {
				event.setCancelled(true);
				return;
			}
			if (game.getGameMode() == GameMode.STARTING) {
				event.setCancelled(true);
				return;
			}

			if (p.getHealth() <= event.getDamage()) {
				event.setCancelled(true);
				game.killPlayer(p, attacker);
			}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerDeathEvent(EntityDamageEvent event) {
		event.setCancelled(false);
		
		if (!(event.getEntity() instanceof Player)) {
			return;
		}
		
		Player p = (Player) event.getEntity();

		int gameid = GameManager.getInstance().getPlayerGameId(p);

		if (gameid == -1) {
			event.setCancelled(true);
			return;
		}
		if(event.getCause() == DamageCause.VOID){
			event.setCancelled(false);
			return;
		}
		
		Game game = GameManager.getInstance().getGame();

		if (game.getGameMode() == GameMode.WAITING) {
			event.setCancelled(true);
			return;
		}
		if (game.getGameMode() == GameMode.STARTING) {
			event.setCancelled(true);
			return;
		}
		
		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayDeath(org.bukkit.event.entity.PlayerDeathEvent event) {
		event.setKeepLevel(true);
		event.setDroppedExp(0);
		event.getEntity().getInventory().clear();
		event.getDrops().clear();
	}

}