package me.epicmiro.pvt.events;

import java.util.regex.Pattern;

import me.epicmiro.pvt.game.Game;
import me.epicmiro.pvt.game.Game.GameMode;
import me.epicmiro.pvt.game.GameManager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.Plugin;

import ru.tehkode.permissions.bukkit.PermissionsEx;

public class ChatEvent implements Listener {

	Plugin p;

	protected static Pattern chatColorPattern = Pattern.compile("(?i)&([0-9A-F])");
	protected static Pattern chatMagicPattern = Pattern.compile("(?i)&([K])");
	protected static Pattern chatBoldPattern = Pattern.compile("(?i)&([L])");
	protected static Pattern chatStrikethroughPattern = Pattern.compile("(?i)&([M])");
	protected static Pattern chatUnderlinePattern = Pattern.compile("(?i)&([N])");
	protected static Pattern chatItalicPattern = Pattern.compile("(?i)&([O])");
	protected static Pattern chatResetPattern = Pattern.compile("(?i)&([R])");

	public ChatEvent(Plugin p) {
		this.p = p;
	}

	@EventHandler
	public void onChat(AsyncPlayerChatEvent e) {
		String message = e.getMessage();
		Player p = e.getPlayer();
		int gameid = GameManager.getInstance().getPlayerGameId(p);
		Game game = GameManager.getInstance().getGame();
		
		if (gameid == -1 || (gameid != -1 && (game.getMode() == GameMode.WAITING || game.getMode() == GameMode.STARTING))) {
			
			String prefix = PermissionsEx.getUser(p).getPrefix();
			prefix = translateColorCodes(prefix);
			
			for (Player pl : Bukkit.getOnlinePlayers()) {
				int gid = GameManager.getInstance().getPlayerGameId(pl);
				Game g = GameManager.getInstance().getGame();
				if (gid == -1 || (gid != -1 && (g.getMode() == GameMode.WAITING || g.getMode() == GameMode.STARTING))) {
					pl.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.RESET + getRank(p) + ChatColor.DARK_GRAY + "] " + ChatColor.DARK_GRAY + prefix + p.getDisplayName() + ": " + ChatColor.RESET + message);
				}
			}
			e.setCancelled(true);
			return;
		}
	

		if (game.isPolice(p)) {
			for (String pl : game.getPolice()) {
				Player pla = Bukkit.getPlayer(pl);
				if (game.isLeader(p)) {
					pla.sendMessage(ChatColor.BLUE + "[Leader] " + p.getName()
							+ ": " + message);
				} else {
					pla.sendMessage(ChatColor.BLUE + p.getName() + ": "
							+ message);
				}
			}
			e.setCancelled(true);
			return;
		} else if (game.isTerrorist(p)) {
			for (String pl : game.getTerrorists()) {
				Player pla = Bukkit.getPlayer(pl);
				if (game.isLeader(p)) {
					pla.sendMessage(ChatColor.RED + "[Leader] " + p.getName()
							+ ": " + message);
				} else {
					pla.sendMessage(ChatColor.RED + p.getName() + ": "
							+ message);
				}
			}
			e.setCancelled(true);
			return;
		} else if (game.isSpectating(p)) {
			for (String pl : game.getSpectators()) {
				Player pla = Bukkit.getPlayer(pl);
				pla.sendMessage(ChatColor.GRAY + p.getName() + ": " + message);
			}
			e.setCancelled(true);
			return;
		} else {
			e.setCancelled(false);
			return;
		}
	}

	private String getRank(Player pl) {
		String rank = "1";
		int level = pl.getLevel();
		if(level < 4){
			rank = "2";
		}else if(level < 8){
			rank = "3";
		}else if(level < 12){
			rank = "4";
		}else if(level < 16){
			rank = "5";
		}else if(level < 20){
			rank = "6";
		}else if(level < 24){
			rank = "7";
		}else if(level < 28){
			rank = "8";
		}else if(level < 32){
			rank = "9";
		}else if(level < 36){
			rank = "10";
		}else if(level < 40){
			rank = "11";
		}else if(level >= 40){
			rank = "12";
		}
		return rank;
	}

	protected String translateColorCodes(String string) {
		if (string == null) {
			return "";
		}

		String newstring = string;
		newstring = chatColorPattern.matcher(newstring).replaceAll("�$1");
		newstring = chatMagicPattern.matcher(newstring).replaceAll("�$1");
		newstring = chatBoldPattern.matcher(newstring).replaceAll("�$1");
		newstring = chatStrikethroughPattern.matcher(newstring).replaceAll(
				"�$1");
		newstring = chatUnderlinePattern.matcher(newstring).replaceAll("�$1");
		newstring = chatItalicPattern.matcher(newstring).replaceAll("�$1");
		newstring = chatResetPattern.matcher(newstring).replaceAll("�$1");
		return newstring;
	}

}
