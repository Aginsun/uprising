package me.epicmiro.pvt.events;

import me.epicmiro.pvt.game.CustomItems;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

public class ItemEventHandler implements Listener
{
	Plugin p;
	
	public ItemEventHandler(Plugin p){
		this.p = p;
	}
	
	@EventHandler
	public void handle( PlayerInteractEvent evt )
	{
		if( evt.getPlayer().getItemInHand() == CustomItems.ITEMMENU )
		{
			//CustomItem.itemActionWithHand( evt.getPlayer(), evt.getAction() );
		}
	}
	
	@EventHandler
	public void handleInteract(PlayerInteractEntityEvent event)
	{
		if( event.getPlayer().getItemInHand() == new ItemStack(Material.PAPER))
		{
			Player player = (Player) event.getRightClicked();
			player.setHealth(player.getHealth() + 5 * 2);
		}
	}
}
