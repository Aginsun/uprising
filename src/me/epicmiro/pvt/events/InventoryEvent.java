package me.epicmiro.pvt.events;

import me.epicmiro.pvt.game.Game;
import me.epicmiro.pvt.game.Game.GameMode;
import me.epicmiro.pvt.game.GameManager;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.plugin.Plugin;
public class InventoryEvent implements Listener {

	Plugin plugin;

	public InventoryEvent(Plugin plugin) {
		this.plugin = plugin;
	}

	

	@EventHandler (ignoreCancelled = true)
	public void onArmorSlot(InventoryClickEvent event) {

		String playername = event.getWhoClicked().getName();
		Player p = Bukkit.getPlayer(playername);
		
		PlayerInventory playerInventory = p.getInventory();
		ItemStack[] contents = playerInventory.getContents();
		
		int gameid = GameManager.getInstance().getPlayerGameId(p);
		
		if (GameManager.getInstance().getPlayerGameId(
				p) == -1) {
			event.setCancelled(false);
			return;
		}

		Game game = GameManager.getInstance().getGame();
		
		if (game.getGameMode() != GameMode.WAITING || game.getGameMode() != GameMode.STARTING){
			event.setCancelled(true);	
			playerInventory.setContents(contents);
			return;
		}
	}

	@EventHandler (ignoreCancelled = true)
	public void inventoryDrop(PlayerDropItemEvent event){
		
		int gameid = GameManager.getInstance().getPlayerGameId(event.getPlayer());
		
		PlayerInventory playerInventory = event.getPlayer().getInventory();
		ItemStack[] contents = playerInventory.getContents();
		
		if (gameid == -1) {
			event.setCancelled(false);
			return;
		}
		Game game = GameManager.getInstance().getGame();
		if (game.getGameMode() != GameMode.WAITING || game.getGameMode() != GameMode.STARTING){
			event.setCancelled(true);	
			playerInventory.setContents(contents);
			return;
		}
	}
}
