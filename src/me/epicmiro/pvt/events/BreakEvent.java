package me.epicmiro.pvt.events;

import java.util.ArrayList;

import me.epicmiro.pvt.game.Game;
import me.epicmiro.pvt.game.GameManager;
import me.epicmiro.pvt.settings.SettingsManager;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.plugin.Plugin;

public class BreakEvent implements Listener {
	Plugin p;
	public ArrayList<Integer> allowedBreak = new ArrayList<Integer>();

	public BreakEvent(Plugin p) {
		this.allowedBreak.addAll(SettingsManager.getInstance().getConfig()
				.getIntegerList("block.break.whitelist"));
		this.p = p;
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onBlockBreak(BlockBreakEvent event) {
		Player p = event.getPlayer();
		int pid = GameManager.getInstance().getPlayerGameId(p);

		if (p.hasPermission("pvt.build.anywhere") && pid == -1) {
			event.setCancelled(false);
			return;
		}
		if (pid == -1) {
			event.setCancelled(true);
			return;
		}

		Game g = GameManager.getInstance().getGame();

		if (g.getMode() == Game.GameMode.DISABLED) {
			return;
		}
		if (g.getMode() != Game.GameMode.INGAME) {
			event.setCancelled(true);
			return;
		}
		System.out.println((event.getBlock().getTypeId()));
		if (!this.allowedBreak.contains(Integer.valueOf(event.getBlock()
				.getTypeId())))
			event.setCancelled(true);
	}
}
