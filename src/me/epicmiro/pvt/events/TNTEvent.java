package me.epicmiro.pvt.events;

import java.util.HashMap;
import java.util.HashSet;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.plugin.Plugin;

public class TNTEvent implements Listener{
	
	Plugin p;
	public static HashMap<Integer, String> tntexplosions = new HashMap<Integer, String>();
	
	public TNTEvent(Plugin p){
		this.p = p;
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onTNTPlace(BlockPlaceEvent e) {
		if(e.getBlock().getType() == Material.TNT){
			Block block = e.getBlockPlaced();
			block.setType(Material.AIR);
			TNTPrimed tnt = block.getWorld().spawn(block.getLocation().add(0.5, 0.5, 0.5), TNTPrimed.class);
			tnt.setFuseTicks(10);
			int id = tnt.getEntityId();
			String player = e.getPlayer().getName();
			TNTEvent.tntexplosions.put(id, player);
			e.setCancelled(false);
			return;
		}else{
			e.setCancelled(false);
			return;
		}
	}
	
	public static Entity[] getNearbyEntities(Location l, int radius) {
		int chunkRadius = radius < 16 ? 1 : (radius - (radius % 16)) / 16;
		HashSet<Entity> radiusEntities = new HashSet<Entity>();
		for (int chX = 0 - chunkRadius; chX <= chunkRadius; chX++) {
			for (int chZ = 0 - chunkRadius; chZ <= chunkRadius; chZ++) {
				int x = (int) l.getX(), y = (int) l.getY(), z = (int) l.getZ();
				for (Entity e : new Location(l.getWorld(), x + (chX * 16), y, z
						+ (chZ * 16)).getChunk().getEntities()) {
					if (e.getLocation().distance(l) <= radius
							&& e.getLocation().getBlock() != l.getBlock())
						radiusEntities.add(e);
				}
			}
		}
		return radiusEntities.toArray(new Entity[radiusEntities.size()]);
	}
}
