package me.epicmiro.pvt.events;

import me.epicmiro.pvt.game.Game;
import me.epicmiro.pvt.game.GameManager;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.Plugin;

public class MoveEvent implements Listener{
	
	Plugin p;
	
	public MoveEvent(Plugin p){
		this.p = p;
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onMove(PlayerMoveEvent e){
		e.setCancelled(false);
		Player p = e.getPlayer();
		int gid = GameManager.getInstance().getPlayerGameId(p);
		if(gid != -1){
			Game g = GameManager.getInstance().getGame();
			if(g.isFrozen()){
				Location from= e.getFrom();
			    Location to= e.getTo();
			    double x=Math.floor(from.getX());
			    double z=Math.floor(from.getZ());
			    if(Math.floor(to.getX())!=x||Math.floor(to.getZ())!=z)
			    {
			        x+=.5;
			        z+=.5;
			        e.getPlayer().teleport(new Location(from.getWorld(),x,from.getY(),z,from.getYaw(),from.getPitch()));
			    }
			}
		}
		e.setCancelled(false);
	}

}
