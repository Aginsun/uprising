package me.epicmiro.pvt.events;

import me.epicmiro.pvt.PvT;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

public class ItemDropEvent implements Listener{
	private PvT plugin;
	
	public ItemDropEvent(PvT instance)
	{
		plugin = instance;
	}
	
	@EventHandler
	public void onPlayerDrop(PlayerDropItemEvent evt)
	{
		if (evt.getItemDrop().getItemStack().getType().equals(Material.DIAMOND))
		{
			evt.setCancelled(true);
		}
	}
}
