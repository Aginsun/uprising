package me.epicmiro.pvt.events;

import java.util.ArrayList;
import java.util.HashMap;

import me.epicmiro.pvt.game.Game;
import me.epicmiro.pvt.game.GameManager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.Plugin;

public class FireBallEvent implements Listener{
	
	Plugin p;
	public static HashMap<Integer, String> fireballs = new HashMap<Integer, String>();
	public static ArrayList<String> cooldowns = new ArrayList<String>();
	
	public FireBallEvent(Plugin p){
		this.p = p;
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onFireRodShoot(final PlayerInteractEvent e){
		
		Player pl = e.getPlayer();
		int gid = GameManager.getInstance().getPlayerGameId(pl);
		if(gid != -1){
			
			Game g = GameManager.getInstance().getGame();
			if(g.isFrozen()){
				e.setCancelled(true);
				return;
			}
			
		if(e.getMaterial() == Material.BLAZE_ROD){
			if(!cooldowns.contains(e.getPlayer().getName())){
			Location loc = e.getPlayer().getEyeLocation().toVector().add(e.getPlayer().getLocation().getDirection().multiply(2)).toLocation(e.getPlayer().getWorld(), e.getPlayer().getLocation().getYaw(), e.getPlayer().getLocation().getPitch());
			Fireball ball = e.getPlayer().getWorld().spawn(loc, Fireball.class);
			int id = ball.getEntityId();
			String player = e.getPlayer().getName();
			FireBallEvent.fireballs.put(id, player);
			cooldowns.add(e.getPlayer().getName());
			e.setCancelled(true);
			Long time = 60L;
			if(e.getPlayer().getLevel() < 20){
				time = 60L;
			}else if(e.getPlayer().getLevel() > 19 && e.getPlayer().getLevel() < 40){
				time = 40L;
			}else if(e.getPlayer().getLevel() >= 40){
				time = 20L;
			}
			Bukkit.getScheduler().scheduleSyncDelayedTask( p, new Runnable(){
				public void run(){
					FireBallEvent.cooldowns.remove(e.getPlayer().getName());
				}
			}, time);
			return;
			}else{
				e.getPlayer().sendMessage(ChatColor.RED + "Please wait for the cooldown.");
			}
		}
		}
	}
	
}
