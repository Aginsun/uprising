package me.epicmiro.pvt.events;

import me.epicmiro.pvt.game.GameManager;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.plugin.Plugin;

public class SpectatorEvent implements Listener {
	Plugin p;

	public SpectatorEvent(Plugin p) {
		this.p = p;
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onBlockPlace(BlockPlaceEvent event) {
		Player player = event.getPlayer();
		if (GameManager.getInstance().isSpectating(player))
			event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onBlockDamage(BlockDamageEvent event) {
		Player player = event.getPlayer();
		if (GameManager.getInstance().isSpectating(player))
			event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onBlockBreak(BlockBreakEvent event) {
		Player player = event.getPlayer();
		if (GameManager.getInstance().isSpectating(player))
			event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onSignChange(PlayerPickupItemEvent event) {
		Player player = event.getPlayer();
		if (GameManager.getInstance().isSpectating(player))
			event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onEntityDamage(EntityDamageByEntityEvent event) {
		Player player = null;
		if ((event.getDamager() instanceof Player))
			player = (Player) event.getDamager();
		else
			return;
		if (GameManager.getInstance().isSpectating(player))
			event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onEntityDamage(EntityDamageEvent event) {
		Player player = null;
		if ((event.getEntity() instanceof Player))
			player = (Player) event.getEntity();
		else
			return;
		if (GameManager.getInstance().isSpectating(player))
			event.setCancelled(true);
	}
}
