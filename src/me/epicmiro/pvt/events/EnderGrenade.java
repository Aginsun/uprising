package me.epicmiro.pvt.events;

import java.util.ArrayList;
import java.util.HashSet;

import me.epicmiro.pvt.game.GameManager;
import net.minecraft.server.v1_6_R2.Packet60Explosion;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_6_R2.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.plugin.Plugin;

public class EnderGrenade implements Listener {

	Plugin p;

	public EnderGrenade(Plugin p) {
		this.p = p;
	}

	public static Entity[] getNearbyEntities(Location l, int radius) {
		int chunkRadius = radius < 16 ? 1 : (radius - (radius % 16)) / 16;
		HashSet<Entity> radiusEntities = new HashSet<Entity>();
		for (int chX = 0 - chunkRadius; chX <= chunkRadius; chX++) {
			for (int chZ = 0 - chunkRadius; chZ <= chunkRadius; chZ++) {
				int x = (int) l.getX(), y = (int) l.getY(), z = (int) l.getZ();
				for (Entity e : new Location(l.getWorld(), x + (chX * 16), y, z
						+ (chZ * 16)).getChunk().getEntities()) {
					if (e.getLocation().distance(l) <= radius
							&& e.getLocation().getBlock() != l.getBlock())
						radiusEntities.add(e);
				}
			}
		}
		return radiusEntities.toArray(new Entity[radiusEntities.size()]);
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void EnderPearlGrenade(final PlayerTeleportEvent e) {
		final Player player = e.getPlayer();

		if (e.getCause() == PlayerTeleportEvent.TeleportCause.ENDER_PEARL) {
			e.setCancelled(true);
			final Location toExplode = e.getTo();

			Bukkit.getScheduler().scheduleSyncDelayedTask(p, new Runnable() {
				public void run() {
					for (Entity p : EnderGrenade
							.getNearbyEntities(toExplode, 7)) {

						if (p instanceof Player) {
							if (GameManager.getInstance().getPlayerGameId(
									(Player) p) != -1
									&& GameManager.getInstance()
											.getPlayerGameId(player) != -1) {
								if ((!GameManager.getInstance().isPlayerPolice(
										(Player) p)
										&& GameManager
												.getInstance()
												.isPlayerPolice((Player) player))
										|| (!GameManager.getInstance()
												.isPlayerTerrorist((Player) p) && GameManager
												.getInstance()
												.isPlayerTerrorist((Player) player))) {
									((Player) p).damage(8, player);
								}
							}
						}
					}
					Packet60Explosion fakeExplosion = new Packet60Explosion();

					fakeExplosion.a = toExplode.getX();
					fakeExplosion.b = toExplode.getY();
					fakeExplosion.c = toExplode.getZ();
					fakeExplosion.d = 3.0F;
					fakeExplosion.e = new ArrayList<Object>();

					for (Player p : player.getWorld().getPlayers()) {
						((CraftPlayer) p).getHandle().playerConnection
								.sendPacket(fakeExplosion);
						p.playSound(toExplode, Sound.EXPLODE, 10F, 1F);
					}
				}
			}, 5L);

		}
	}
}
