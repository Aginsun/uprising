package me.epicmiro.pvt.events;

import java.sql.SQLException;

import me.epicmiro.pvt.settings.ExpManager;
import me.epicmiro.pvt.settings.SettingsManager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.Plugin;

public class JoinEvent implements Listener {

	Plugin plugin;

	public JoinEvent(Plugin plugin) {
		this.plugin = plugin;
	}

	@EventHandler (priority = EventPriority.MONITOR)
	public void playerJoin(PlayerJoinEvent event) {
		// On player join send them the message from config.yml
		event.getPlayer().sendMessage(
				ChatColor.LIGHT_PURPLE + "=================================");
		event.getPlayer().sendMessage(
				ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD
						+ plugin.getConfig().getString("message"));
		event.getPlayer().sendMessage(
				ChatColor.LIGHT_PURPLE + "=================================");
		
		Player p = event.getPlayer();
		p.getInventory().clear();
		IconMenuOpenEvent.giveClassSelectItem(p);
		try
		{
			ExpManager.getInstance().PlayerJoin(p);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		for (Player pl : Bukkit.getOnlinePlayers()) {
			pl.showPlayer(p);
		}
		
		ExpManager.getInstance().updateExpBar(p);
		p.teleport(SettingsManager.getInstance().getLobbySpawn());
	}
}
