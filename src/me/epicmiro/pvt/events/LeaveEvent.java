package me.epicmiro.pvt.events;

import me.epicmiro.pvt.game.Game;
import me.epicmiro.pvt.game.GameManager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;

public class LeaveEvent implements Listener {

	Plugin plugin;

	public LeaveEvent(Plugin plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void playerQuit(PlayerQuitEvent event) {
		Player p = event.getPlayer();
		int gameid = GameManager.getInstance().getPlayerGameId(p);
		if (gameid != -1) {
			Game game = GameManager.getInstance().getGame();
			game.playerLeave(p);
			p.setAllowFlight(false);
			p.setFlying(false);
			for (Player pl : Bukkit.getOnlinePlayers()) {
				pl.showPlayer(p);
			}
			
			for (String pl : game.getAllPlayers()) {
				Player pla = Bukkit.getPlayer(pl);
				pla.sendMessage(ChatColor.AQUA + p.getDisplayName()
						+ " has left the arena.");
			}
		}
	}

}
