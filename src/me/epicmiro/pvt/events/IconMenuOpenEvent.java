package me.epicmiro.pvt.events;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.epicmiro.pvt.PvT;
import me.epicmiro.pvt.game.Game;
import me.epicmiro.pvt.game.GameManager;

public class IconMenuOpenEvent implements Listener
{
	
	private PvT plugin;
	private static ItemStack stack;
	
	public IconMenuOpenEvent(PvT instance)
	{
		plugin = instance;
	}
	
	//Method to give player Class Selection Item
	public static void giveClassSelectItem (Player player)
	{
		stack = new ItemStack(Material.DIAMOND);
		ItemMeta itemMeta = stack.getItemMeta();
		itemMeta.setDisplayName(ChatColor.GOLD + "Class Selection");
		stack.setItemMeta(itemMeta);
		player.getInventory().setItem(8, stack);
	}
	
	//Listener for right Click Event
	@EventHandler
	public void onRightClick(PlayerInteractEvent event)
	{
		if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK)
		{
			if ( (event.getItem() != null) && event.getItem().getType() == Material.DIAMOND)
			{
				Game game = GameManager.getInstance().getGame();
				game.setClasses(event.getPlayer());
			}
		}
	}
}
