package me.epicmiro.pvt.game;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class CustomItems 
{
	public static ItemStack ITEMMENU;
	
	public static void setCustomItems()
	{
		ArrayList<String> lore = new ArrayList<String>();
		ITEMMENU = new ItemStack(Material.DIAMOND);
		ItemMeta meta = ITEMMENU.getItemMeta();
		meta.setDisplayName(ChatColor.GOLD +"Class Buying");
		lore.clear();
		lore.add(ChatColor.WHITE + "Right-click to use.");
		meta.setLore(lore);
		ITEMMENU.setItemMeta(meta);
	}
}
