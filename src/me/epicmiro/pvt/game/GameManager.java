package me.epicmiro.pvt.game;

import java.util.ArrayList;
import java.util.List;

import me.epicmiro.pvt.PvT;
import me.epicmiro.pvt.game.Game.GameMode;
import me.epicmiro.pvt.settings.SettingsManager;
import me.epicmiro.pvt.tasks.GameCountDown;


import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.bukkit.selections.Selection;

public class GameManager {
	static GameManager instance = new GameManager();
	private Game game;
	private List<Player> playersInGame = new ArrayList<Player>();
	private PvT p;
	public GameCountDown countdown;
	
	public static GameManager getInstance() {
		return instance;
	}

	public void setup(PvT plugin) {
		this.p = plugin;
		LoadGames();
	}

	public Plugin getPlugin() {
		return this.p;
	}

	public void reloadGames() {
		LoadGames();
	}

	public void LoadGames() {
		FileConfiguration c = SettingsManager.getInstance().getSystemConfig();
		this.game = null;
		
			if (c.isSet("pvt.arena.x1")) {
				if (c.getBoolean("pvt.arena.enabled")) {
					System.out.println("Loading Arena");
					game = new Game(p);
					countdown = new GameCountDown(p);
					countdown.start();
				}
			}
	}

	public int getBlockGameId(Location v) {
			if (this.game.isBlockInArena(v)) {
				return 1;
			}
			
		return -1;
	}
	
	public int getPlayerGameId(Player p) {
		if (this.game.isInGame(p)) {
			return 1;
		}
		
	return -1;
}

	public boolean isPlayerPolice(Player player) {
			if (this.game.isPolice(player)) {
				return true;
			}
		return false;
	}
	
	public boolean isSpectating(Player player) {

			if (this.game.isSpectating(player)) {
				return true;
			}
		return false;
	}

	public boolean isPlayerTerrorist(Player player) {
			if (this.game.isTerrorist(player)) {
				return true;
			}
		return false;
	}

	public Game getGame() {
		return this.game;
	}

	public void removePlayer(Player p) {
		removePlayerRefrence(p);
	}

	public void disableGame(int id) {
		getGame().disable();
	}

	public void enableGame(int id) {
		getGame().enable();
	}

	public Game.GameMode getGameMode(int a) {
				return this.game.getMode();
	}

	public void addPlayer(Player p) {
		Game game = getGame();
		if (game == null) {
			p.sendMessage(ChatColor.RED + "Game does not exist");
			return;
		}
		getGame().addPlayer(p);
	}

	public WorldEditPlugin getWorldEdit() {
		return this.p.getWorldEdit();
	}

	public void createArenaFromSelection(Player pl) {
		FileConfiguration c = SettingsManager.getInstance().getSystemConfig();
		SettingsManager s = SettingsManager.getInstance();

		WorldEditPlugin we = this.p.getWorldEdit();
		Selection sel = we.getSelection(pl);
		if (sel == null) {
			pl.sendMessage(ChatColor.RED
					+ "You must make a WorldEdit Selection first");
			return;
		}
		Location max = sel.getMaximumPoint();
		Location min = sel.getMinimumPoint();

		s.getSpawns().set("spawns", null);
		c.set("pvt.arena..world", max.getWorld().getName());
		c.set("pvt.arena.x1", Integer.valueOf(max.getBlockX()));
		c.set("pvt.arena.y1", Integer.valueOf(max.getBlockY()));
		c.set("pvt.arena.z1", Integer.valueOf(max.getBlockZ()));
		c.set("pvt.arena.x2", Integer.valueOf(min.getBlockX()));
		c.set("pvt.arena.y2", Integer.valueOf(min.getBlockY()));
		c.set("pvt.arena.z2", Integer.valueOf(min.getBlockZ()));
		c.set("pvt.arena.enabled", Boolean.valueOf(true));

		s.saveSystemConfig();

		AddArena();

		pl.sendMessage(ChatColor.GREEN + "Arena Succesfully added");
	}

	private void AddArena() {
		Game game = new Game(p);
		this.game = game;
	}

	public void removeArena(int no) {
		this.game = null;
	}

	public void gameEndCallBack() {
		getGame().setRBStatus("Cleaning up.");
		clearPlayerCache();
		Game game = getGame();
		countdown.start();
		game.setMode(GameMode.WAITING);
	}

	public void clearPlayerCache() {
		for(Player p : this.playersInGame)
				this.playersInGame.remove(p);
	}

	public void removePlayerRefrence(Player p) {
		if (this.playersInGame.contains(p)) {
			this.playersInGame.remove(p);
		}
	}
}