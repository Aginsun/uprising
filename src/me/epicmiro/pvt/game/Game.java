package me.epicmiro.pvt.game;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import me.epicmiro.pvt.events.IconMenuOpenEvent;
import me.epicmiro.pvt.settings.ExpManager;
import me.epicmiro.pvt.settings.SettingsManager;
import net.arkipelego.economy.EconomyManager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;

public class Game {
	public GameMode mode = GameMode.DISABLED;

	private List<Player> queue = new ArrayList<Player>();
	private Map<String, String> players = new HashMap<String, String>();
	public Map<String, String> playerjobs = new HashMap<String, String>();

	private Arena arena;
	private boolean isfrozen;
	private int gameId;

	private FileConfiguration c;
	private FileConfiguration s;

	private ArrayList<Integer> policeSpawns = new ArrayList<Integer>();
	private ArrayList<Integer> terroristSpawns = new ArrayList<Integer>();

	private HashMap<Player, ItemStack[][]> inv_store = new HashMap<Player, ItemStack[][]>();

	private int policeSpawnCount = 0;
	private int terroristSpawnCount = 0;

	private boolean disabled = false;
	private double rbpercent = 0.0D;
	private String rbstatus = "";

	Plugin plugin;

	int counttime = 0;
	int threadsync = 0;

	public Game(Plugin p) {
		this.c = SettingsManager.getInstance().getConfig();
		this.s = SettingsManager.getInstance().getSystemConfig();
		setup(p);
	}

	public void setup(Plugin p) {
		this.mode = GameMode.LOADING;
		this.plugin = p;
		this.gameId = 1;
		int x = this.s.getInt("pvt.arena.x1");
		int y = this.s.getInt("pvt.arena.y1");
		int z = this.s.getInt("pvt.arena.z1");

		int x1 = this.s.getInt("pvt.arena.x2");
		int y1 = this.s.getInt("pvt.arena.y2");
		int z1 = this.s.getInt("pvt.arena.z2");

		Location max = new Location(SettingsManager.getGameWorld(), Math.max(x,
				x1), Math.max(y, y1), Math.max(z, z1));

		Location min = new Location(SettingsManager.getGameWorld(), Math.min(x,
				x1), Math.min(y, y1), Math.min(z, z1));

		this.arena = new Arena(min, max);

		loadspawns("police");
		loadspawns("terrorist");

		this.mode = GameMode.WAITING;
	}

	public void loadspawns(String group) {

		if (group == "police") {
			for (int a = 1; a <= SettingsManager.getInstance().getSpawnCount(
					"police"); a++) {
				this.policeSpawns.add(Integer.valueOf(a));
				this.policeSpawnCount = a;
			}
		} else if (group == "terrorist") {
			for (int a = 1; a <= SettingsManager.getInstance().getSpawnCount(
					"terrorist"); a++) {
				this.terroristSpawns.add(Integer.valueOf(a));
				this.terroristSpawnCount = a;
			}
		} else {
			System.out.println("Spawns failed to load.");
		}

	}

	public void addTerroristSpawn() {
		this.terroristSpawnCount += 1;
		this.terroristSpawns.add(Integer.valueOf(this.terroristSpawnCount));
	}

	public void addPoliceSpawn() {
		this.policeSpawnCount += 1;
		this.policeSpawns.add(Integer.valueOf(this.policeSpawnCount));
	}

	public void setMode(GameMode m) {
		this.mode = m;
	}

	public GameMode getGameMode() {
		return this.mode;
	}

	public Arena getArena() {
		return this.arena;
	}

	public void enable() {
		this.mode = GameMode.WAITING;
		this.disabled = false;
	}

	public boolean addPlayer(Player p) {
		if (!isInGame(p)) {
			this.players.put(p.getName(), null);
			return true;
		} else {
			return false;
		}
	}

	public int getRandomTerroristSpawn() {
		Random r = new Random();
		int no = SettingsManager.getInstance().getSpawnCount(
				"terrorist");

		if (no == 1) {
			return 1;
		} else {
			int random = r.nextInt(SettingsManager.getInstance().getSpawnCount(
					"terrorist") - 1);
			return random + 1;
		}
	}

	public int getRandomPoliceSpawn() {
		Random r = new Random();
		int no = SettingsManager.getInstance().getSpawnCount(
				"police");

		if (no == 1) {
			return 1;
		} else {
			int random = r.nextInt(SettingsManager.getInstance().getSpawnCount(
					 "police") - 1);
			return random + 1;
		}
	}

	public void setClasses(Player player) {
		//getEquipment(getTerroristLeader(), "leader");
		//getEquipment(getPoliceLeader(), "leader");

		/*for (Player player : plugin.getServer().getOnlinePlayers()/*this.players.keySet()) 
		{
			*/
			String pl = player.getName();
			IconMenu menu = new IconMenu(Bukkit.getPlayer(pl).getDisplayName(), 9, new IconMenu.OptionClickEventHandler() 
			{
				@Override
				public void onOptionClick(IconMenu.OptionClickEvent event) 
				{
					event.setWillClose(false);
					Game game = GameManager.getInstance().getGame();
					if (event.getPosition() == 0)
					{
						if (!game.playerjobs.containsKey(event.getPlayer().getName()))
						{
							event.getPlayer().sendMessage("You have chosen the class: "	+ event.getName());
							game.playerjobs.put(event.getPlayer().getName(), "soldier");
							event.setWillDestroy(true);

						}
						event.setWillClose(true);
					}

					if (event.getPosition() == 1) {
						if (!game.playerjobs.containsKey(event.getPlayer().getName()))
						{
							if(ExpManager.getInstance().doesPlayerOwnClass(event.getPlayer(), "medic")) {
								event.getPlayer().sendMessage("You have chosen the class: " + event.getName());
								game.playerjobs.put(event.getPlayer().getName(), "medic");
								event.setWillDestroy(true);
							} else {
								try {
									//Check if player can afford class
									boolean canAfford = false;
									canAfford = EconomyManager.instance().canAfford((OfflinePlayer) event.getPlayer(), 800);
									
									if (canAfford && EconomyManager.instance().isRegistered((OfflinePlayer) event.getPlayer())) {
										EconomyManager.instance().decreaseBalance((OfflinePlayer) event.getPlayer(), 800);
										ExpManager.addClasses((Player) event.getPlayer(), "medic");
										event.getPlayer().sendMessage("You have bought this class.");
									} else {
										event.getPlayer().sendMessage("You cannot afford this class.");
									}
								} catch(Exception e) {}
							}
						}
						
						event.setWillClose(true);

					}

					if (event.getPosition() == 2) {
						if (!game.playerjobs.containsKey(event.getPlayer().getName())) 
						{
							if(ExpManager.getInstance().doesPlayerOwnClass(event.getPlayer(), "sniper")) {
								event.getPlayer().sendMessage("You have chosen the class: " + event.getName());
								game.playerjobs.put(event.getPlayer().getName(), "sniper");
								event.setWillDestroy(true);
							} else {
								try {
									//Check if player can afford class
									boolean canAfford = false;
									canAfford = EconomyManager.instance().canAfford((OfflinePlayer) event.getPlayer(), 800);
									
									if (canAfford && EconomyManager.instance().isRegistered((OfflinePlayer) event.getPlayer())) {
										EconomyManager.instance().decreaseBalance((OfflinePlayer) event.getPlayer(), 800);
										ExpManager.addClasses((Player) event.getPlayer(), "sniper");
										event.getPlayer().sendMessage("You have bought this class.");
									} else {
										event.getPlayer().sendMessage("You cannot afford this class.");
									}
								} catch(Exception e) {}
							}
						}
						event.setWillClose(true);
					}

					if (event.getPosition() == 3)
					{
						if (!game.playerjobs.containsKey(event.getPlayer().getName()))
						{
							if(ExpManager.getInstance().doesPlayerOwnClass(event.getPlayer(), "pyro")) {
								event.getPlayer().sendMessage("You have chosen the class: " + event.getName());
								game.playerjobs.put(event.getPlayer().getName(), "pyro");
								event.setWillDestroy(true);
							} else {
								try {
									//Check if player can afford class
									boolean canAfford = false;
									canAfford = EconomyManager.instance().canAfford((OfflinePlayer) event.getPlayer(), 1600);
								
									if (canAfford && EconomyManager.instance().isRegistered((OfflinePlayer) event.getPlayer())) {
										EconomyManager.instance().decreaseBalance((OfflinePlayer) event.getPlayer(), 1600);
										ExpManager.addClasses((Player) event.getPlayer(), "pyro");
										event.getPlayer().sendMessage("You have bought this class.");
									} else {
										event.getPlayer().sendMessage("You cannot afford this class.");
									}
								} catch(Exception e) {}
							}
						}
						event.setWillClose(true);
					}

					if (event.getPosition() == 4)
					{	
						if (!game.playerjobs.containsKey(event.getPlayer().getName())) 
						{
							if (ExpManager.getInstance().doesPlayerOwnClass(event.getPlayer(), "bomber")) {
								event.getPlayer().sendMessage("You have chosen the class: " + event.getName());	
								game.playerjobs.put(event.getPlayer().getName(), "bomber");
								event.setWillDestroy(true);
							} else {
								try {
									//Check if player can afford class
									boolean canAfford = false;
									canAfford = EconomyManager.instance().canAfford((OfflinePlayer) event.getPlayer(), 1600);
								
									if (canAfford && EconomyManager.instance().isRegistered((OfflinePlayer) event.getPlayer())) {
										EconomyManager.instance().decreaseBalance((OfflinePlayer) event.getPlayer(), 1600);
										ExpManager.addClasses((Player) event.getPlayer(), "bomber");
										event.getPlayer().sendMessage("You have bought this class.");
									} else {
										event.getPlayer().sendMessage("You cannot afford this class.");
									}
								} catch(Exception e) {}
							}
						}
						event.setWillClose(true);
					}
				}
			}, plugin);

			if (!this.playerjobs.containsKey(pl)) {
				Player p = Bukkit.getPlayer(pl);
				//getBasicSuit(p);
				if (p.getLevel() > 39) {
					menu.setOption(0, new ItemStack(Material.STONE_SWORD, 1), "Soldier", 
							ChatColor.GOLD + "- Stone Sword (Sharpness II)",
							ChatColor.GOLD + "- 2 Grenade", 
							ChatColor.GOLD + "- 2 Health Potion", 
							ChatColor.GOLD + "- Protection II Armour");
					
					if(ExpManager.getInstance().doesPlayerOwnClass(p, "medic")) {
						menu.setOption(1, new ItemStack(Material.PAPER, 1),	"Medic", 
							ChatColor.GOLD + "- 1 Wooden Sword (Knockback III)", 
							ChatColor.GOLD + "- 5 Health Potion",
							ChatColor.GOLD + "- 5 Bandages", 
							ChatColor.GOLD + "- Thorns III Armour");
					} else {
						menu.setOption(1, new ItemStack(Material.PAPER, 1),	"Medic", 
							ChatColor.RED + "Unlock for 800 credits",
							ChatColor.RED + "- 1 Wooden Sword (Knockback III)", 
							ChatColor.RED + "- 5 Health Potion",
							ChatColor.RED + "- 5 Bandages", 
							ChatColor.RED + "- Thorns III Armour");
					}
					
					if(ExpManager.getInstance().doesPlayerOwnClass(p, "sniper")) {
						menu.setOption(2, new ItemStack(Material.BOW, 1), "Sniper",
							ChatColor.GOLD + "- Bow (Infinity I + Power II)",
							ChatColor.GOLD + "- 1 Wooden Sword (Knockback III)",
							ChatColor.GOLD + "- 1 Arrow", 
							ChatColor.GOLD + "- 2 Health Potion");
					} else {
						menu.setOption(2, new ItemStack(Material.BOW, 1), "Sniper",
							ChatColor.RED + "Unlock for 800 credits",
							ChatColor.RED + "- Bow (Infinity I + Power II)",
							ChatColor.RED + "- 1 Wooden Sword (Knockback III)",
							ChatColor.RED + "- 1 Arrow", 
							ChatColor.RED + "- 2 Health Potion");
					}

					if (ExpManager.getInstance().doesPlayerOwnClass(p, "pyro")) {
						menu.setOption(3, new ItemStack(Material.FIRE, 1), "Pyro", 
							ChatColor.GOLD + "- Fireball Rod (1 sec cooldown)",
							ChatColor.GOLD + "- Stone Sword (Fire Aspect II)",
							ChatColor.GOLD + "- 1 Grenade", 
							ChatColor.GOLD + "- 1 Health Potion", 
							ChatColor.GOLD + "- 3 Bread", 
							ChatColor.GOLD + "- Fire Protection III Armour");
					} else {
						menu.setOption(3, new ItemStack(Material.FIRE, 1), "Pyro", 
							ChatColor.RED + "Unlock for 1600 credits",
							ChatColor.RED + "- Fireball Rod (1 sec cooldown)",
							ChatColor.RED + "- Stone Sword (Fire Aspect II)",
							ChatColor.RED + "- 1 Grenade", 
							ChatColor.RED + "- 1 Health Potion", 
							ChatColor.RED + "- 3 Bread", 
							ChatColor.RED + "- Fire Protection III Armour");
					}
					
					if(ExpManager.getInstance().doesPlayerOwnClass(p, "bomber")) {
						menu.setOption(4, new ItemStack(Material.ENDER_PEARL, 1), "Bomb Specialist",
							ChatColor.GOLD + "- 3 TNT",
							ChatColor.GOLD + "- 5 Grenades",
							ChatColor.GOLD + "- 1 Health Potion",
							ChatColor.GOLD + "- 3 Bread",
							ChatColor.GOLD + "- Wooden Sword (Knockback III)",
							ChatColor.GOLD + "- Blast Protection III + Protection I Armour");
					} else {
						menu.setOption(4, new ItemStack(Material.ENDER_PEARL, 1), "Bomb Specialist",
							ChatColor.RED + "Unlock for 1600 credits",
							ChatColor.RED + "- 3 TNT",
							ChatColor.RED + "- 5 Grenades",
							ChatColor.RED + "- 1 Health Potion",
							ChatColor.RED + "- 3 Bread",
							ChatColor.RED + "- Wooden Sword (Knockback III)",
							ChatColor.RED + "- Blast Protection III + Protection I Armour");
					}
				} else if (p.getLevel() > 19 && p.getLevel() < 40) {
						menu.setOption(0, new ItemStack(Material.STONE_SWORD, 1), "Soldier", 
							ChatColor.GOLD + "- Stone Sword (Sharpness I)",
							ChatColor.GOLD + "- 1 Grenade", 
							ChatColor.GOLD + "- 2 Health Potion", 
							ChatColor.GOLD + "- Protection I Armour");

					if (ExpManager.getInstance().doesPlayerOwnClass(p, "medic")) {
						menu.setOption(1, new ItemStack(Material.PAPER, 1),	"Medic", 
							ChatColor.GOLD + "- 1 Wooden Sword (Knockback I)",
							ChatColor.GOLD + "- 4 Health Potion",
							ChatColor.GOLD + "- 5 Bandages",
							ChatColor.GOLD + "- Thorns II Armour");
					} else {
						menu.setOption(1, new ItemStack(Material.PAPER, 1),	"Medic", 
							ChatColor.RED + "Unlock for 800 credits",
							ChatColor.RED + "- 1 Wooden Sword (Knockback I)",
							ChatColor.RED + "- 4 Health Potion",
							ChatColor.RED + "- 5 Bandages",
							ChatColor.RED + "- Thorns II Armour");
					}
					
					if(ExpManager.getInstance().doesPlayerOwnClass(p, "sniper")) {
						menu.setOption(2, new ItemStack(Material.BOW, 1), "Sniper",
							ChatColor.GOLD + "- Bow (Infinity I + Power I)",
							ChatColor.GOLD + "- 1 Wooden Sword (Knockback I)",
							ChatColor.GOLD + "- 1 Arrow", 
							ChatColor.GOLD + "- 1 Health Potion");
					} else {
						menu.setOption(2, new ItemStack(Material.BOW, 1), "Sniper",
							ChatColor.RED + "Unlock for 800 credits",
							ChatColor.RED + "- Bow (Infinity I + Power I)",
							ChatColor.RED + "- 1 Wooden Sword (Knockback I)",
							ChatColor.RED + "- 1 Arrow", 
							ChatColor.RED + "- 1 Health Potion");
					}
					
					if(ExpManager.getInstance().doesPlayerOwnClass(p, "pyro")) {
						menu.setOption(3, new ItemStack(Material.FIRE, 1), "Pyro", 
							ChatColor.GOLD + "- Fireball Rod (2 sec cooldown)",
							ChatColor.GOLD + "- Stone Sword (Fire Aspect I)",
							ChatColor.GOLD + "- 1 Grenade", 
							ChatColor.GOLD + "- Fire Protection III Armour");
					} else {
						menu.setOption(3, new ItemStack(Material.FIRE, 1), "Pyro", 
							ChatColor.RED + "Unlock for 1600 credits",
							ChatColor.RED + "- Fireball Rod (2 sec cooldown)",
							ChatColor.RED + "- Stone Sword (Fire Aspect I)",
							ChatColor.RED + "- 1 Grenade", 
							ChatColor.RED + "- Fire Protection III Armour");
					}
					
					if(ExpManager.getInstance().doesPlayerOwnClass(p, "bomber")) {
						menu.setOption(4, new ItemStack(Material.ENDER_PEARL, 1), "Bomb Specialist",
							ChatColor.GOLD + "- 3 TNT",
							ChatColor.GOLD + "- 5 Grenades",
							ChatColor.GOLD + "- 3 Bread",
							ChatColor.GOLD + "- Wooden Sword (Knockback II)",
							ChatColor.GOLD + "- Blast Protection II + Protection I Armour");
					} else {
						menu.setOption(4, new ItemStack(Material.ENDER_PEARL, 1), "Bomb Specialist",
							ChatColor.RED + "Unlock for 1600 credits",
							ChatColor.RED + "- 3 TNT",
							ChatColor.RED + "- 5 Grenades",
							ChatColor.RED + "- 3 Bread",
							ChatColor.RED + "- Wooden Sword (Knockback II)",
							ChatColor.RED + "- Blast Protection II + Protection I Armour");
					}
					
				} else {
						menu.setOption(0, new ItemStack(Material.STONE_SWORD, 1), "Soldier", 
							ChatColor.GOLD + "- Stone Sword",
							ChatColor.GOLD + "- 1 Grenade", 
							ChatColor.GOLD + "- 1 Health Potion");

					if(ExpManager.getInstance().doesPlayerOwnClass(p, "medic")) {
						menu.setOption(1, new ItemStack(Material.PAPER, 1), "Medic", 
							ChatColor.GOLD + "- 1 Wooden Sword",
							ChatColor.GOLD + "- 3 Health Potion",
							ChatColor.GOLD + "- 5 Bandages", 
							ChatColor.GOLD + "- Thorns I Armour");
					} else {
						menu.setOption(1, new ItemStack(Material.PAPER, 1),	"Medic", 
							ChatColor.RED + "Unlock for 800 credits",
							ChatColor.RED + "- 1 Wooden Sword",
							ChatColor.RED + "- 3 Health Potion",
							ChatColor.RED + "- 5 Bandages", 
							ChatColor.RED + "- Thorns I Armour");
					}
					
					if(ExpManager.getInstance().doesPlayerOwnClass(p, "sniper")) {
						menu.setOption(2, new ItemStack(Material.BOW, 1), "Sniper",
							ChatColor.GOLD + "- Bow (Infinity I)",
							ChatColor.GOLD + "- 1 Wooden Sword", 
							ChatColor.GOLD + "- 1 Arrow");
					} else {
						menu.setOption(2, new ItemStack(Material.BOW, 1), "Sniper",
							ChatColor.RED + "Unlock for 800 credits",
							ChatColor.RED + "- Bow (Infinity I)",
							ChatColor.RED + "- 1 Wooden Sword", 
							ChatColor.RED + "- 1 Arrow");
					}
					
					if(ExpManager.getInstance().doesPlayerOwnClass(p, "pyro")) {
						menu.setOption(3, new ItemStack(Material.FIRE, 1), "Pyro", 
							ChatColor.GOLD + "- Fireball Rod (3 sec cooldown)",
							ChatColor.GOLD + "- Stone Sword",
							ChatColor.GOLD + "- 1 Grenade", 
							ChatColor.GOLD + "- 3 Bread", 
							ChatColor.GOLD + "- Fire Protection II Armour");
					} else {
						menu.setOption(3, new ItemStack(Material.FIRE, 1), "Pyro", 
							ChatColor.RED + "Unlock for 1600 credits",
							ChatColor.RED + "- Fireball Rod (3 sec cooldown)",
							ChatColor.RED + "- Stone Sword",
							ChatColor.RED + "- 1 Grenade", 
							ChatColor.RED + "- 3 Bread", 
							ChatColor.RED + "- Fire Protection II Armour");
					}
						
					if(ExpManager.getInstance().doesPlayerOwnClass(p, "bomber")) {
						menu.setOption(4, new ItemStack(Material.ENDER_PEARL, 1), "Bomb Specialist",
							ChatColor.GOLD + "- 3 TNT",
							ChatColor.GOLD + "- Wooden Sword",
							ChatColor.GOLD + "- 3 Grenades",
							ChatColor.GOLD + "- 3 Bread",
							ChatColor.GOLD + "- Blast Protection II + Protection I Armour");
					} else {
						menu.setOption(4, new ItemStack(Material.ENDER_PEARL, 1), "Bomb Specialist",
							ChatColor.RED + "Unlock for 1600 credits",
							ChatColor.RED + "- 3 TNT",
							ChatColor.RED + "- Wooden Sword",
							ChatColor.RED + "- 3 Grenades",
							ChatColor.RED + "- 3 Bread",
							ChatColor.RED + "- Blast Protection II + Protection I Armour");
					}
				}
			menu.open(p);

		}
	}

	@SuppressWarnings("deprecation")
	public void getBasicSuit(Player p) {

		ItemStack helm = getHat(p);
		p.getInventory().setHelmet(helm);

		ItemStack chest = getChestplate(p);
		p.getInventory().setChestplate(chest);

		ItemStack pants = getPants(p);
		p.getInventory().setLeggings(pants);

		ItemStack boots = getBoots(p);
		p.getInventory().setBoots(boots);

		p.updateInventory();
	}

	@SuppressWarnings("deprecation")
	public void getEquipment(String name, String job) {
		Player p = Bukkit.getPlayer(name);
		p.getInventory().clear();

		if (p.getLevel() < 20) {
			
			if (job == "soldier") {
				ItemStack helm = getHat(p);
				p.getInventory().setHelmet(helm);

				ItemStack chest = getChestplate(p);
				p.getInventory().setChestplate(chest);

				ItemStack pants = getPants(p);
				p.getInventory().setLeggings(pants);

				ItemStack boots = getBoots(p);
				p.getInventory().setBoots(boots);

				ItemStack sword = getSword(p, Material.STONE_SWORD);
				p.getInventory().addItem(sword);

				ItemStack pearl = getEnderGrenade(p, 1);
				p.getInventory().addItem(pearl);

				ItemStack potion = getHealPotions(p, 1);
				p.getInventory().addItem(potion);

			} else if (job == "medic") {
				ItemStack helm = getHat(p);
				helm.addEnchantment(Enchantment.THORNS, 1);
				p.getInventory().setHelmet(helm);

				ItemStack chest = getChestplate(p);
				chest.addEnchantment(Enchantment.THORNS, 1);
				p.getInventory().setChestplate(chest);

				ItemStack pants = getPants(p);
				pants.addEnchantment(Enchantment.THORNS, 1);
				p.getInventory().setLeggings(pants);

				ItemStack boots = getBoots(p);
				boots.addEnchantment(Enchantment.THORNS, 1);
				p.getInventory().setBoots(boots);

				ItemStack sword = getSword(p, Material.WOOD_SWORD);
				p.getInventory().addItem(sword);

				ItemStack potion = getHealPotions(p, 3);
				p.getInventory().addItem(potion);

				ItemStack bandage = getBandages(p, 5);
				p.getInventory().addItem(bandage);

			} else if (job == "sniper") {

				ItemStack helm = getHat(p);
				p.getInventory().setHelmet(helm);

				ItemStack chest = getChestplate(p);
				p.getInventory().setChestplate(chest);

				ItemStack pants = getPants(p);
				p.getInventory().setLeggings(pants);

				ItemStack boots = getBoots(p);
				p.getInventory().setBoots(boots);

				ItemStack sword = getSword(p, Material.WOOD_SWORD);
				p.getInventory().addItem(sword);

				ItemStack bow = getBow(p);
				p.getInventory().addItem(bow);

				p.getInventory().addItem(new ItemStack(Material.ARROW));

			} else if (job == "pyro") {
				ItemStack helm = getHat(p);
				p.getInventory().setHelmet(helm);

				ItemStack chest = getChestplate(p);
				chest.addEnchantment(Enchantment.PROTECTION_FIRE, 2);
				p.getInventory().setChestplate(chest);

				ItemStack pants = getPants(p);
				p.getInventory().setLeggings(pants);

				ItemStack boots = getBoots(p);
				p.getInventory().setBoots(boots);

				ItemStack sword = getSword(p, Material.STONE_SWORD);
				p.getInventory().addItem(sword);

				ItemStack wand = getWand(p);
				p.getInventory().addItem(wand);

				ItemStack pearl = getEnderGrenade(p, 1);
				p.getInventory().addItem(pearl);

				p.getInventory().addItem(new ItemStack(Material.BREAD, 3));
			} else if (job == "bomber") {
				ItemStack helm = getHat(p);
				p.getInventory().setHelmet(helm);

				ItemStack chest = getChestplate(p);
				chest.addEnchantment(Enchantment.PROTECTION_EXPLOSIONS, 2);
				chest.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL,
						1);
				p.getInventory().setChestplate(chest);

				ItemStack pants = getPants(p);
				p.getInventory().setLeggings(pants);

				ItemStack boots = getBoots(p);
				p.getInventory().setBoots(boots);

				ItemStack sword = getSword(p, Material.WOOD_SWORD);
				p.getInventory().addItem(sword);

				p.getInventory().addItem(new ItemStack(Material.TNT, 3));

				ItemStack pearl = getEnderGrenade(p, 3);
				p.getInventory().addItem(pearl);

				p.getInventory().addItem(new ItemStack(Material.BREAD, 3));
			} else if (job == "leader") {

				ItemStack helm = getHat(p);
				p.getInventory().setHelmet(helm);

				ItemStack chest = getChestplate(p);
				p.getInventory().setChestplate(chest);

				ItemStack pants = getPants(p);
				p.getInventory().setLeggings(pants);

				ItemStack boots = getBoots(p);
				p.getInventory().setBoots(boots);

				ItemStack sword = getSword(p, Material.STONE_SWORD);
				p.getInventory().addItem(sword);

				ItemStack pearl = getEnderGrenade(p, 2);
				p.getInventory().addItem(pearl);

				ItemStack pot = getHealPotions(p, 3);
				p.getInventory().addItem(pot);

				p.getInventory().addItem(new ItemStack(Material.MELON, 3));

			}
		} else if (p.getLevel() > 19 && p.getLevel() < 40) {
			if (job == "soldier") {
				ItemStack helm = getHat(p);
				p.getInventory().setHelmet(helm);

				ItemStack chest = getChestplate(p);
				chest.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
				p.getInventory().setChestplate(chest);

				ItemStack pants = getPants(p);
				p.getInventory().setLeggings(pants);

				ItemStack boots = getBoots(p);
				p.getInventory().setBoots(boots);

				ItemStack sword = getSword(p, Material.STONE_SWORD);
				sword.addEnchantment(Enchantment.DAMAGE_ALL, 1);
				p.getInventory().addItem(sword);

				ItemStack pearl = getEnderGrenade(p, 1);
				p.getInventory().addItem(pearl);

				ItemStack potion = getHealPotions(p, 2);
				p.getInventory().addItem(potion);

			} else if (job == "medic") {
				ItemStack helm = getHat(p);
				helm.addEnchantment(Enchantment.THORNS, 2);
				p.getInventory().setHelmet(helm);

				ItemStack chest = getChestplate(p);
				chest.addEnchantment(Enchantment.THORNS, 2);
				p.getInventory().setChestplate(chest);

				ItemStack pants = getPants(p);
				pants.addEnchantment(Enchantment.THORNS, 2);
				p.getInventory().setLeggings(pants);

				ItemStack boots = getBoots(p);
				boots.addEnchantment(Enchantment.THORNS, 2);
				p.getInventory().setBoots(boots);

				ItemStack sword = getSword(p, Material.WOOD_SWORD);
				p.getInventory().addItem(sword);

				ItemStack potion = getHealPotions(p, 4);
				p.getInventory().addItem(potion);

				ItemStack bandage = getBandages(p, 5);
				p.getInventory().addItem(bandage);

			} else if (job == "sniper") {

				ItemStack helm = getHat(p);
				p.getInventory().setHelmet(helm);

				ItemStack chest = getChestplate(p);
				p.getInventory().setChestplate(chest);

				ItemStack pants = getPants(p);
				p.getInventory().setLeggings(pants);

				ItemStack boots = getBoots(p);
				p.getInventory().setBoots(boots);

				ItemStack sword = getSword(p, Material.WOOD_SWORD);
				sword.addEnchantment(Enchantment.KNOCKBACK, 1);
				p.getInventory().addItem(sword);

				ItemStack bow = getBow(p);
				bow.addEnchantment(Enchantment.ARROW_DAMAGE, 1);
				p.getInventory().addItem(bow);

				ItemStack potion = getHealPotions(p, 1);
				p.getInventory().addItem(potion);

				p.getInventory().addItem(new ItemStack(Material.ARROW));

			} else if (job == "pyro") {
				
				ItemStack helm = getHat(p);
				p.getInventory().setHelmet(helm);

				ItemStack chest = getChestplate(p);
				chest.addEnchantment(Enchantment.PROTECTION_FIRE, 3);
				p.getInventory().setChestplate(chest);

				ItemStack pants = getPants(p);
				p.getInventory().setLeggings(pants);

				ItemStack boots = getBoots(p);
				p.getInventory().setBoots(boots);

				ItemStack sword = getSword(p, Material.STONE_SWORD);
				sword.addEnchantment(Enchantment.FIRE_ASPECT, 1);
				p.getInventory().addItem(sword);

				ItemStack wand = getWand(p);
				p.getInventory().addItem(wand);

				ItemStack pearl = getEnderGrenade(p, 1);
				p.getInventory().addItem(pearl);

				p.getInventory().addItem(new ItemStack(Material.BREAD, 3));

			} else if (job == "bomber") {
				ItemStack helm = getHat(p);
				p.getInventory().setHelmet(helm);

				ItemStack chest = getChestplate(p);
				chest.addEnchantment(Enchantment.PROTECTION_EXPLOSIONS, 2);
				chest.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL,
						1);
				p.getInventory().setChestplate(chest);

				ItemStack pants = getPants(p);
				p.getInventory().setLeggings(pants);

				ItemStack boots = getBoots(p);
				p.getInventory().setBoots(boots);

				ItemStack sword = getSword(p, Material.WOOD_SWORD);
				sword.addEnchantment(Enchantment.KNOCKBACK, 2);
				p.getInventory().addItem(sword);

				p.getInventory().addItem(new ItemStack(Material.TNT, 3));

				ItemStack pearl = getEnderGrenade(p, 5);
				p.getInventory().addItem(pearl);

				p.getInventory().addItem(new ItemStack(Material.BREAD, 3));
			} else if (job == "leader") {

				ItemStack helm = getHat(p);
				p.getInventory().setHelmet(helm);

				ItemStack chest = getChestplate(p);
				p.getInventory().setChestplate(chest);

				ItemStack pants = getPants(p);
				p.getInventory().setLeggings(pants);

				ItemStack boots = getBoots(p);
				p.getInventory().setBoots(boots);

				ItemStack sword = getSword(p, Material.STONE_SWORD);
				sword.addEnchantment(Enchantment.DAMAGE_ALL, 1);
				sword.addEnchantment(Enchantment.KNOCKBACK, 1);
				p.getInventory().addItem(sword);

				ItemStack bow = getBow(p);
				p.getInventory().addItem(bow);

				ItemStack pearl = getEnderGrenade(p, 2);
				p.getInventory().addItem(pearl);

				ItemStack pot = getHealPotions(p, 3);
				p.getInventory().addItem(pot);

				ItemStack melon = new ItemStack(Material.MELON, 3);
				p.getInventory().addItem(melon);

				p.getInventory().addItem(new ItemStack(Material.ARROW));

			}
		} else if (p.getLevel() > 39) {
			if (job == "soldier") {
				ItemStack helm = getHat(p);
				p.getInventory().setHelmet(helm);

				ItemStack chest = getChestplate(p);
				chest.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
				p.getInventory().setChestplate(chest);

				ItemStack pants = getPants(p);
				p.getInventory().setLeggings(pants);

				ItemStack boots = getBoots(p);
				p.getInventory().setBoots(boots);

				ItemStack sword = getSword(p, Material.STONE_SWORD);
				sword.addEnchantment(Enchantment.DAMAGE_ALL, 2);
				p.getInventory().addItem(sword);

				ItemStack pearl = getEnderGrenade(p, 2);
				p.getInventory().addItem(pearl);

				ItemStack potion = getHealPotions(p, 2);
				p.getInventory().addItem(potion);

			} else if (job == "medic") {
				ItemStack helm = getHat(p);
				helm.addEnchantment(Enchantment.THORNS, 3);
				p.getInventory().setHelmet(helm);

				ItemStack chest = getChestplate(p);
				chest.addEnchantment(Enchantment.THORNS, 3);
				p.getInventory().setChestplate(chest);

				ItemStack pants = getPants(p);
				pants.addEnchantment(Enchantment.THORNS, 3);
				p.getInventory().setLeggings(pants);

				ItemStack boots = getBoots(p);
				boots.addEnchantment(Enchantment.THORNS, 3);
				p.getInventory().setBoots(boots);

				ItemStack sword = getSword(p, Material.WOOD_SWORD);
				sword.addEnchantment(Enchantment.KNOCKBACK, 3);
				p.getInventory().addItem(sword);

				ItemStack potion = getHealPotions(p, 5);
				p.getInventory().addItem(potion);

				ItemStack bandage = getBandages(p, 5);
				p.getInventory().addItem(bandage);

			} else if (job == "sniper") {

				ItemStack helm = getHat(p);
				p.getInventory().setHelmet(helm);

				ItemStack chest = getChestplate(p);
				p.getInventory().setChestplate(chest);

				ItemStack pants = getPants(p);
				p.getInventory().setLeggings(pants);

				ItemStack boots = getBoots(p);
				p.getInventory().setBoots(boots);

				ItemStack bow = getBow(p);
				bow.addEnchantment(Enchantment.ARROW_DAMAGE, 2);
				p.getInventory().addItem(bow);

				ItemStack sword = getSword(p, Material.WOOD_SWORD);
				sword.addEnchantment(Enchantment.KNOCKBACK, 2);
				p.getInventory().addItem(sword);

				ItemStack potion = getHealPotions(p, 2);
				p.getInventory().addItem(potion);

				p.getInventory().addItem(new ItemStack(Material.ARROW));

			} else if (job == "pyro") {
				ItemStack helm = getHat(p);
				p.getInventory().setHelmet(helm);

				ItemStack chest = getChestplate(p);
				chest.addEnchantment(Enchantment.PROTECTION_FIRE, 3);
				p.getInventory().setChestplate(chest);

				ItemStack pants = getPants(p);
				p.getInventory().setLeggings(pants);

				ItemStack boots = getBoots(p);
				p.getInventory().setBoots(boots);

				ItemStack sword = getSword(p, Material.STONE_SWORD);
				sword.addEnchantment(Enchantment.FIRE_ASPECT, 2);
				p.getInventory().addItem(sword);

				ItemStack wand = getWand(p);
				p.getInventory().addItem(wand);

				ItemStack pearl = getEnderGrenade(p, 1);
				p.getInventory().addItem(pearl);

				ItemStack potion = getHealPotions(p, 1);
				p.getInventory().addItem(potion);

				p.getInventory().addItem(new ItemStack(Material.BREAD, 3));

			} else if (job == "bomber") {
				ItemStack helm = getHat(p);
				p.getInventory().setHelmet(helm);

				ItemStack chest = getChestplate(p);
				chest.addEnchantment(Enchantment.PROTECTION_EXPLOSIONS, 3);
				chest.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL,
						1);
				p.getInventory().setChestplate(chest);

				ItemStack pants = getPants(p);
				p.getInventory().setLeggings(pants);

				ItemStack boots = getBoots(p);
				p.getInventory().setBoots(boots);

				ItemStack sword = getSword(p, Material.WOOD_SWORD);
				sword.addEnchantment(Enchantment.KNOCKBACK, 3);
				p.getInventory().addItem(sword);

				p.getInventory().addItem(new ItemStack(Material.TNT, 3));

				ItemStack pearl = getEnderGrenade(p, 5);
				p.getInventory().addItem(pearl);

				ItemStack potion = getHealPotions(p, 1);
				p.getInventory().addItem(potion);

				p.getInventory().addItem(new ItemStack(Material.BREAD, 3));
			} else if (job == "leader") {

				ItemStack helm = getHat(p);
				p.getInventory().setHelmet(helm);

				ItemStack chest = getChestplate(p);
				p.getInventory().setChestplate(chest);

				ItemStack pants = getPants(p);
				p.getInventory().setLeggings(pants);

				ItemStack boots = getBoots(p);
				p.getInventory().setBoots(boots);

				ItemStack sword = getSword(p, Material.STONE_SWORD);
				sword.addEnchantment(Enchantment.DAMAGE_ALL, 2);
				sword.addEnchantment(Enchantment.KNOCKBACK, 2);
				p.getInventory().addItem(sword);

				ItemStack bow = getBow(p);
				bow.addEnchantment(Enchantment.ARROW_DAMAGE, 2);
				p.getInventory().addItem(bow);

				ItemStack pearl = getEnderGrenade(p, 3);
				p.getInventory().addItem(pearl);

				ItemStack pot = getHealPotions(p, 3);
				p.getInventory().addItem(pot);

				ItemStack melon = new ItemStack(Material.MELON, 3);
				p.getInventory().addItem(melon);

				p.getInventory().addItem(new ItemStack(Material.ARROW));

			}
		}
		p.updateInventory();

	}

	public ItemStack getEnderGrenade(Player p, int amount) {
		if (isTerrorist(p)) {
			ItemStack pearl = new ItemStack(Material.ENDER_PEARL);
			ItemMeta pearlmeta = pearl.getItemMeta();
			pearlmeta.setDisplayName("Ender Grenade");
			ArrayList<String> pearldescription = new ArrayList<String>();
			pearldescription
					.add("The scientists recently invented these ender grenades,");
			pearldescription.add("they explode in a small radius when thrown.");
			pearldescription
					.add("Some of these seem to be missing from the armoury.");
			pearlmeta.setLore(pearldescription);
			pearl.setItemMeta(pearlmeta);
			pearl.setAmount(amount);

			return pearl;
		} else {
			ItemStack pearl = new ItemStack(Material.ENDER_PEARL);
			ItemMeta pearlmeta = pearl.getItemMeta();
			pearlmeta.setDisplayName("Ender Grenade");
			ArrayList<String> pearldescription = new ArrayList<String>();
			pearldescription
					.add("The scientists recently invented these ender grenades,");
			pearldescription.add("they explode in a small radius when thrown.");
			pearldescription.add("These ender grenades seem to be stolen.");
			pearlmeta.setLore(pearldescription);
			pearl.setItemMeta(pearlmeta);
			pearl.setAmount(amount);

			return pearl;
		}
	}

	public ItemStack getBandages(Player p, int amount) {
		ItemStack bandage = new ItemStack(Material.PAPER);
		ItemMeta bandagemeta = bandage.getItemMeta();
		bandagemeta.setDisplayName("Bandage");
		ArrayList<String> bandagedescription = new ArrayList<String>();
		bandagedescription.add("Use this to heal your teammates or yourself.");
		bandagedescription.add("- Left Click: Heal your teammate");
		bandagedescription.add("- Right Click: Heal yourself.");
		bandagemeta.setLore(bandagedescription);
		bandage.setItemMeta(bandagemeta);
		bandage.setAmount(amount);
		return bandage;
	}

	public ItemStack getHealPotions(Player p, int amount) {
		Potion potion = new Potion(PotionType.INSTANT_HEAL, 1);
		ItemStack potionstack = potion.toItemStack(amount);
		return potionstack;
	}

	public ItemStack getBow(Player p) {
		ItemStack bow = new ItemStack(Material.BOW);
		ItemMeta bowmeta = bow.getItemMeta();
		bowmeta.setDisplayName("Sniper");
		ArrayList<String> bowdescription = new ArrayList<String>();
		bowdescription.add("Used for long range killing.");
		bowmeta.setLore(bowdescription);
		bow.setItemMeta(bowmeta);
		bow.addEnchantment(Enchantment.ARROW_INFINITE, 1);
		return bow;

	}

	public ItemStack getSword(Player p, Material type) {
		if (isTerrorist(p)) {
			if (isLeader(p)) {
				ItemStack sword = new ItemStack(type);
				ItemMeta meta = sword.getItemMeta();
				meta.setDisplayName("Leader's Sabre");
				ArrayList<String> description = new ArrayList<String>();
				description
						.add("An high quality sabre that only the leader posesses.");
				meta.setLore(description);
				sword.setItemMeta(meta);
				return sword;
			} else {
				ItemStack sword = new ItemStack(type);
				ItemMeta meta = sword.getItemMeta();
				if (type == Material.WOOD_SWORD) {
					meta.setDisplayName("Wooden Blade");
				} else if (type == Material.STONE_SWORD) {
					meta.setDisplayName("Stone Sword");
				}
				ArrayList<String> description = new ArrayList<String>();
				if (type == Material.WOOD_SWORD) {
					description
							.add("A wooden blade just strong enough to damage someone.");
				} else if (type == Material.STONE_SWORD) {
					description
							.add("A stone sword which can deal moderate amounts of damage.");
				}
				meta.setLore(description);
				sword.setItemMeta(meta);
				return sword;
			}
		} else {
			if (isLeader(p)) {
				ItemStack sword = new ItemStack(type);
				ItemMeta meta = sword.getItemMeta();
				meta.setDisplayName("Leader's Sabre");
				ArrayList<String> description = new ArrayList<String>();
				description
						.add("An high quality sabre that only the leader posesses.");
				meta.setLore(description);
				sword.setItemMeta(meta);
				return sword;
			} else {
				ItemStack sword = new ItemStack(type);
				ItemMeta meta = sword.getItemMeta();
				if (type == Material.WOOD_SWORD) {
					meta.setDisplayName("Wooden Blade");
				} else if (type == Material.STONE_SWORD) {
					meta.setDisplayName("Stone Sword");
				}
				ArrayList<String> description = new ArrayList<String>();
				if (type == Material.WOOD_SWORD) {
					description
							.add("A wooden blade just strong enough to damage someone.");
				} else if (type == Material.STONE_SWORD) {
					description
							.add("A stone sword which can deal moderate amounts of damage.");
				}
				meta.setLore(description);
				sword.setItemMeta(meta);

				return sword;
			}
		}
	}

	public ItemStack getBoots(Player p) {
		if (isTerrorist(p)) {
			ItemStack boots = new ItemStack(Material.LEATHER_BOOTS);
			LeatherArmorMeta metaboots = (LeatherArmorMeta) boots.getItemMeta();
			metaboots.setColor(Color.ORANGE);
			metaboots.setDisplayName("Terrorist Sandals");
			ArrayList<String> bootsdescription = new ArrayList<String>();
			bootsdescription
					.add("The sandals look like they have been worn for ages.");
			metaboots.setLore(bootsdescription);
			boots.setItemMeta(metaboots);
			return boots;
		} else {
			ItemStack boots = new ItemStack(Material.LEATHER_BOOTS);
			LeatherArmorMeta metaboots = (LeatherArmorMeta) boots.getItemMeta();
			metaboots.setColor(Color.BLUE);
			metaboots.setDisplayName("Police Boots");
			ArrayList<String> bootsdescription = new ArrayList<String>();
			bootsdescription
					.add("Blue rubber boots, perfect for long distances.");
			metaboots.setLore(bootsdescription);
			boots.setItemMeta(metaboots);
			return boots;
		}
	}

	public ItemStack getPants(Player p) {
		if (isTerrorist(p)) {
			ItemStack pants = new ItemStack(Material.LEATHER_LEGGINGS);
			LeatherArmorMeta metapants = (LeatherArmorMeta) pants.getItemMeta();
			metapants.setColor(Color.ORANGE);
			metapants.setDisplayName("Terrorist Pants");
			ArrayList<String> pantsdescription = new ArrayList<String>();
			pantsdescription.add("A pair of pants with some holes in it.");
			metapants.setLore(pantsdescription);
			pants.setItemMeta(metapants);
			return pants;
		} else {
			ItemStack pants = new ItemStack(Material.LEATHER_LEGGINGS);
			LeatherArmorMeta metapants = (LeatherArmorMeta) pants.getItemMeta();
			metapants.setColor(Color.BLUE);
			metapants.setDisplayName("Police Pants");
			ArrayList<String> pantsdescription = new ArrayList<String>();
			pantsdescription
					.add("These pants look like they dont rip that easily.");
			metapants.setLore(pantsdescription);
			pants.setItemMeta(metapants);
			return pants;
		}
	}

	public ItemStack getChestplate(Player p) {
		if (isTerrorist(p)) {
			ItemStack chest = new ItemStack(Material.LEATHER_CHESTPLATE);
			LeatherArmorMeta metachest = (LeatherArmorMeta) chest.getItemMeta();
			metachest.setColor(Color.ORANGE);
			metachest.setDisplayName("Terrorist Top");
			ArrayList<String> chestdescription = new ArrayList<String>();
			chestdescription
					.add("An shredded T-Shirt that doesnt look like it will last long.");
			metachest.setLore(chestdescription);
			chest.setItemMeta(metachest);
			return chest;
		} else {
			ItemStack chest = new ItemStack(Material.LEATHER_CHESTPLATE);
			LeatherArmorMeta metachest = (LeatherArmorMeta) chest.getItemMeta();
			metachest.setColor(Color.BLUE);
			metachest.setDisplayName("Police Armoured Vest");
			ArrayList<String> chestdescription = new ArrayList<String>();
			chestdescription
					.add("This vest looks like it has protected against a few bullets.");
			metachest.setLore(chestdescription);
			chest.setItemMeta(metachest);
			return chest;
		}
	}

	public ItemStack getHat(Player p) {
		if (isTerrorist(p)) {
			ItemStack helm = new ItemStack(Material.LEATHER_HELMET);
			LeatherArmorMeta metahelm = (LeatherArmorMeta) helm.getItemMeta();
			metahelm.setColor(Color.ORANGE);
			metahelm.setDisplayName("Terrorist Hat");
			ArrayList<String> helmdescription = new ArrayList<String>();
			helmdescription.add("A protection against the heat");
			metahelm.setLore(helmdescription);
			helm.setItemMeta(metahelm);
			return helm;
		} else {
			ItemStack helm = new ItemStack(Material.LEATHER_HELMET);
			LeatherArmorMeta metahelm = (LeatherArmorMeta) helm.getItemMeta();
			metahelm.setColor(Color.BLUE);
			metahelm.setDisplayName("Police Hat");
			ArrayList<String> helmdescription = new ArrayList<String>();
			helmdescription.add("A hat worn by the best police members.");
			metahelm.setLore(helmdescription);
			helm.setItemMeta(metahelm);
			return helm;
		}
	}

	public ItemStack getWand(Player p) {
		ItemStack wand = new ItemStack(Material.BLAZE_ROD);
		ItemMeta wandmeta = wand.getItemMeta();
		wandmeta.setDisplayName("Fireball Wand");
		ArrayList<String> wanddescription = new ArrayList<String>();
		wanddescription.add("A magical wand used to shoot fireballs.");
		wandmeta.setLore(wanddescription);
		wand.setItemMeta(wandmeta);
		return wand;
	}

	public void removeFromQueue(Player p) {
		this.queue.remove(p);
	}

	@SuppressWarnings("deprecation")
	public void playerLeave(Player p) {

		this.players.remove(p.getName());
		this.playerjobs.remove(p.getName());

		GameManager.getInstance().removePlayerRefrence(p);

		if (!p.isOnline())
			if (this.getMode() != GameMode.WAITING
					|| this.getMode() != GameMode.STARTING) {
				restoreInvOffline(p.getName());
			} else {
				p.closeInventory();
				if (this.getMode() != GameMode.WAITING
						|| this.getMode() != GameMode.STARTING) {
					restoreInv(p);
				}
				p.teleport(SettingsManager.getInstance().getLobbySpawn());
			}

		for (Player pla : Bukkit.getOnlinePlayers()) {
			pla.showPlayer(p);
		}

		if ((getTerroristCount() == 0 || getPoliceCount() == 0)
				&& this.getGameMode() != GameMode.DISABLED
				&& this.getGameMode() != GameMode.RESETING
				&& this.getGameMode() != GameMode.WAITING
				&& this.getGameMode() != GameMode.STARTING) {
			this.decideWin();
		}
		p.updateInventory();
	}

	@SuppressWarnings("deprecation")
	public void playerDead(Player p) {

		if (isTerrorist(p)) {
			this.players.put(p.getName(), "deadterrorist");
		} else {
			this.players.put(p.getName(), "deadpolice");
		}
		this.playerjobs.remove(p.getName());

		if (getTerroristCount() == 0 || getPoliceCount() == 0) {
			this.decideWin();
			return;
		}

		if (!p.isOnline())
			restoreInvOffline(p.getName());
		else {
			clearInv(p);

			for (Player pl : Bukkit.getOnlinePlayers()) {
				pl.hidePlayer(p);
			}
			p.setAllowFlight(true);
			p.setFlying(true);
			p.closeInventory();
			if (getTerroristCount() != 0 || getPoliceCount() != 0) {
				p.sendMessage(ChatColor.LIGHT_PURPLE
						+ "You are now a spectator until the game ends.");
			}

			// restoreInv(p);
			// p.teleport(SettingsManager.getInstance().getLobbySpawn());
		}
		p.updateInventory();
	}

	public boolean isLeader(Player p) {
		if (this.playerjobs.get(p.getName()) == "leader") {
			return true;
		}
		return false;
	}

	public void killPlayer(Player p, Player attacker) {
		if (this.mode != GameMode.WAITING) {

			p.setHealth(20);
			p.setFoodLevel(20);
			p.setFireTicks(0);
			ExpManager.getInstance().updateExpBar(p);

			if (isLeader(p)) {
				if (isTerrorist(p)) {

					if (attacker != null) {
						ExpManager.getInstance().AddPlayerExp(attacker, 3);
						ExpManager.getInstance().addMoneyToCache(attacker, 2);
						/*
						try {
							EconomyManager.instance().increaseBalance(p, 2);
						} catch (SQLException e) {
							e.printStackTrace();
						}
						*/
						attacker.sendMessage(ChatColor.GREEN + ""
								+ ChatColor.ITALIC + "You have gained 3 exp.");
						ExpManager.getInstance().AddDeath(p);
						ExpManager.getInstance().AddKill(attacker);
					}

					for (String pl : this.players.keySet()) {
						Player pla = Bukkit.getPlayer(pl);
						if (attacker != null) {
							// attacker leader bonus points
							pla.sendMessage(ChatColor.GOLD
									+ "The terrorist leader has been killed by "
									+ attacker.getDisplayName() + "!");
						} else {
							pla.sendMessage(ChatColor.GOLD
									+ "The terrorist leader has died!");
						}
					}
					playerDead(p);
				} else if (isPolice(p)) {

					if (attacker != null) {
						ExpManager.getInstance().AddPlayerExp(attacker, 3);
						ExpManager.getInstance().addMoneyToCache(attacker, 2);
						/*
						try {
							EconomyManager.instance().increaseBalance(p, 2);
						} catch (SQLException e) {
							e.printStackTrace();
						}
						*/
						attacker.sendMessage(ChatColor.GREEN + ""
								+ ChatColor.ITALIC + "You have gained 3 exp.");
						ExpManager.getInstance().AddDeath(p);
						ExpManager.getInstance().AddKill(attacker);
					}

					for (String pl : this.players.keySet()) {
						Player pla = Bukkit.getPlayer(pl);
						if (attacker != null) {
							// attacker leader bonus points
							pla.sendMessage(ChatColor.BLUE
									+ "The police leader has been killed by "
									+ attacker.getDisplayName() + "!");
						} else {
							pla.sendMessage(ChatColor.BLUE
									+ "The police leader has died!");
						}

					}
					playerDead(p);
				}
			} else {
				p.sendMessage(ChatColor.RED + "You have died.");
				if (isTerrorist(p)) {

					if (IsTerroristLeaderKilled()) {
						playerDead(p);

						if (attacker != null) {
							ExpManager.getInstance().AddPlayerExp(attacker, 2);
							ExpManager.getInstance().addMoneyToCache(attacker, 2);
							/*
							try {
								EconomyManager.instance().increaseBalance(p, 2);
							} catch (SQLException e) {
								e.printStackTrace();
							}
							*/
							attacker.sendMessage(ChatColor.GREEN + ""
									+ ChatColor.ITALIC
									+ "You have gained 2 exp.");
							ExpManager.getInstance().AddDeath(p);
							ExpManager.getInstance().AddKill(attacker);
						}

						for (String pl : getAllPlayers()) {
							Player pla = Bukkit.getPlayer(pl);
							if (p.getName() != pl) {
								if (attacker != null) {
									pla.sendMessage(ChatColor.GOLD
											+ p.getDisplayName()
											+ " has been killed by "
											+ attacker.getDisplayName() + ".");
								} else {
									pla.sendMessage(ChatColor.GOLD
											+ p.getDisplayName() + " has died.");
								}
							}
						}
					} else {
						if (attacker != null) {
							ExpManager.getInstance().AddPlayerExp(attacker, 1);
							ExpManager.getInstance().addMoneyToCache(attacker, 2);
							/*
							try {
								EconomyManager.instance().increaseBalance(p, 2);
							} catch (SQLException e) {
								e.printStackTrace();
							}
							*/
							attacker.sendMessage(ChatColor.GREEN + ""
									+ ChatColor.ITALIC
									+ "You have gained 1 exp.");
							ExpManager.getInstance().AddDeath(p);
							ExpManager.getInstance().AddKill(attacker);
						}
						p.sendMessage("Respawning...");
						getEquipment(p.getName(),
								this.playerjobs.get(p.getName()));
						p.teleport(getRandomTerroristSpawnPoint());
					}

				} else if (isPolice(p)) {
					if (IsPoliceLeaderKilled()) {
						playerDead(p);
						if (attacker != null) {
							ExpManager.getInstance().AddPlayerExp(attacker, 2);
							ExpManager.getInstance().addMoneyToCache(attacker, 2);
							/*
							try {
								EconomyManager.instance().increaseBalance(p, 2);
							} catch (SQLException e) {
								e.printStackTrace();
							}
							*/
							attacker.sendMessage(ChatColor.GREEN + ""
									+ ChatColor.ITALIC
									+ "You have gained 2 exp.");
							ExpManager.getInstance().AddDeath(p);
							ExpManager.getInstance().AddKill(attacker);
						}
						for (String pl : getAllPlayers()) {
							Player pla = Bukkit.getPlayer(pl);
							if (p.getName() != pl) {
								if (attacker != null) {
									pla.sendMessage(ChatColor.BLUE
											+ p.getDisplayName()
											+ " has been killed by "
											+ attacker.getDisplayName() + ".");
								} else {
									pla.sendMessage(ChatColor.BLUE
											+ p.getDisplayName() + " has died.");
								}
							}
						}
					} else {
						if (attacker != null) {
							ExpManager.getInstance().AddPlayerExp(attacker, 1);
							ExpManager.getInstance().addMoneyToCache(attacker, 2);
							/*
							try {
								EconomyManager.instance().increaseBalance(p, 2);
							} catch (SQLException e) {
								e.printStackTrace();
							}
							*/
							attacker.sendMessage(ChatColor.GREEN + ""
									+ ChatColor.ITALIC
									+ "You have gained 1 exp.");
							ExpManager.getInstance().AddDeath(p);
							ExpManager.getInstance().AddKill(attacker);
						}
						p.sendMessage("Respawning...");
						getEquipment(p.getName(),
								this.playerjobs.get(p.getName()));
						p.teleport(getRandomPoliceSpawnPoint());
					}
				}
			}
			// TagAPI.refreshPlayer(p);
		}
	}

	public void setPolice(Player p) {
		this.players.put(p.getName(), "police");
		// TagAPI.refreshPlayer(p);
	}

	public boolean IsPoliceLeaderKilled() {
		boolean is = true;
		for (Entry<String, String> p : this.playerjobs.entrySet()) {
			if (p.getValue() == "leader"
					&& isPolice(Bukkit.getPlayer(p.getKey()))) {
				is = false;
			}
		}
		return is;
	}

	public boolean IsTerroristLeaderKilled() {
		boolean is = true;
		for (Entry<String, String> p : this.playerjobs.entrySet()) {
			if (p.getValue() == "leader"
					&& isTerrorist(Bukkit.getPlayer(p.getKey()))) {
				is = false;
			}
		}
		return is;
	}

	public void setTerrorist(Player p) {
		this.players.put(p.getName(), "terrorist");
		// TagAPI.refreshPlayer(p);
	}

	public void decideWin() {
		if (GameMode.DISABLED == this.mode)
			return;

		if (getPoliceCount() >= 1) {

			for (Entry<String, String> pl : this.players.entrySet()) {
				Player p = Bukkit.getPlayer(pl.getKey());
				if (pl.getValue() == "police" || pl.getValue() == "deadpolice") {
					ExpManager.getInstance().AddPlayerExp(p, 5);
					p.sendMessage(ChatColor.GREEN
							+ ""
							+ ChatColor.ITALIC
							+ "You have gained 5 exp by being in the winning team.");
				} else {
					p.sendMessage(ChatColor.GREEN
							+ ""
							+ ChatColor.ITALIC
							+ "You have gained 2 exp for being in the losing team.");
				}
				if (p != null) {
					p.setGameMode(org.bukkit.GameMode.ADVENTURE);
					// p.sendMessage(ChatColor.DARK_AQUA + "The police won!");
					p.teleport(SettingsManager.getInstance().getLobbySpawn());
					p.setHealth(20);
					p.setFoodLevel(20);
					p.setFireTicks(0);
					p.setAllowFlight(false);
					p.setFlying(false);
					p.closeInventory();
					for (Player pla : Bukkit.getOnlinePlayers()) {
						pla.showPlayer(p);
					}
				}
				clearInv(p);
				IconMenuOpenEvent.giveClassSelectItem(p);
				
			}
			Bukkit.broadcastMessage(ChatColor.BLUE
					+ "======================================");
			Bukkit.broadcastMessage(ChatColor.BLUE + "" + ChatColor.BOLD
					+ "The police has won!");
			Bukkit.broadcastMessage(ChatColor.BLUE
					+ "======================================");

		} else if (getTerroristCount() >= 1) {

			for (Entry<String, String> pl : this.players.entrySet()) {
				Player p = Bukkit.getPlayer(pl.getKey());
				if (pl.getValue() == "terrorist"
						|| pl.getValue() == "deadterrorist") {
					ExpManager.getInstance().AddPlayerExp(p, 5);
					p.sendMessage(ChatColor.GREEN
							+ ""
							+ ChatColor.ITALIC
							+ "You have gained 5 exp for being in the winning team.");
				} else {
					p.sendMessage(ChatColor.GREEN
							+ ""
							+ ChatColor.ITALIC
							+ "You have gained 2 exp for being in the losing team.");
				}
				
				if (p != null) {
					p.setGameMode(org.bukkit.GameMode.ADVENTURE);
					// p.sendMessage(ChatColor.DARK_AQUA +
					// "The terrorists won!");
					p.teleport(SettingsManager.getInstance().getLobbySpawn());
					p.setHealth(20);
					p.setFoodLevel(20);
					p.setFireTicks(0);
					p.setAllowFlight(false);
					p.setFlying(false);
					p.closeInventory();
					for (Player pla : Bukkit.getOnlinePlayers()) {
						pla.showPlayer(p);
					}
				}
				clearInv(p);
				IconMenuOpenEvent.giveClassSelectItem(p);
			}

			Bukkit.broadcastMessage(ChatColor.RED
					+ "======================================");
			Bukkit.broadcastMessage(ChatColor.RED + "" + ChatColor.BOLD
					+ "The terrorists have won!");
			Bukkit.broadcastMessage(ChatColor.RED
					+ "======================================");
		}

		this.mode = GameMode.FINISHING;

		setRBStatus("Saving Game");
		setRBStatus("clearing spawns");
		this.policeSpawns.clear();
		this.terroristSpawns.clear();
		setRBStatus("loading spawns");
		loadspawns("police");
		loadspawns("terrorist");

		this.players.clear();
		this.playerjobs.clear();

		setRBStatus("clearing players");

		endGame();
	}


	public void endGame() {
		this.mode = GameMode.WAITING;
		try {
			ExpManager.getInstance().updateAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		resetArena();
	}

	public void disable() {
		this.disabled = true;
		this.terroristSpawns.clear();
		this.policeSpawns.clear();

		this.mode = GameMode.DISABLED;
		for (int a = 0; a < this.players.size(); a = 0)
			try {
				Object pl = this.players.keySet().toArray()[a];
				String playername = (String) pl;
				Player p = Bukkit.getPlayer(playername);
				p.sendMessage(ChatColor.RED + "Game disabled");
				clearInv(p);
				playerLeave(p);
			} catch (Exception localException) {
			}

		this.queue.clear();
		resetArena();
		GameManager.getInstance().clearPlayerCache();
	}

	public void resetArena() {
		this.mode = GameMode.RESETING;
		setRBStatus("starting");
		GameManager.getInstance().gameEndCallBack();
		GameReset r = new GameReset(this);
		r.resetArena();
	}

	public void resetCallback() {
		if (!this.disabled)
			enable();
		else
			this.mode = GameMode.DISABLED;
	}

	public void messageAll(String msg) {
		for (String p : getAllPlayers()) {
			Player pl = Bukkit.getPlayer(p);
			pl.sendMessage(msg);
		}
	}

	public void saveInv(Player p) {
		ItemStack[][] store = new ItemStack[2][1];

		store[0] = p.getInventory().getContents();
		store[1] = p.getInventory().getArmorContents();

		this.inv_store.put(p, store);
	}

	public void restoreInvOffline(String p) {
		restoreInv(Bukkit.getPlayer(p));
	}

	@SuppressWarnings("deprecation")
	public void restoreInv(Player p) {
		try {
			if (getGameMode() != GameMode.WAITING
					|| getGameMode() != GameMode.STARTING) {
				clearInv(p);
				p.getInventory().setContents(
						((ItemStack[][]) this.inv_store.get(p))[0]);
				p.getInventory().setArmorContents(
						((ItemStack[][]) this.inv_store.get(p))[1]);
				this.inv_store.remove(p);
				p.updateInventory();
			}
		} catch (Exception localException) {
		}
	}

	@SuppressWarnings("deprecation")
	public void clearInv(Player p) {
		p.getInventory().clear();
		p.getInventory().setBoots(new ItemStack(Material.AIR));
		p.getInventory().setLeggings(new ItemStack(Material.AIR));
		p.getInventory().setChestplate(new ItemStack(Material.AIR));
		p.getInventory().setHelmet(new ItemStack(Material.AIR));
		p.updateInventory();
	}

	public void startGame() {
		if (this.mode == GameMode.INGAME) {
			return;
		}

		if (this.players.size() < SettingsManager.getInstance().getAutoStart()) {
			for (String pl : this.players.keySet()) {
				Player p = Bukkit.getPlayer(pl);
				p.sendMessage(ChatColor.RED + "Not Enough Players!");
			}
			return;
		}
		setFrozen(true);
		Bukkit.broadcastMessage(ChatColor.LIGHT_PURPLE
				+ "======================================");
		Bukkit.broadcastMessage(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD
				+ "The game has started.");
		Bukkit.broadcastMessage(ChatColor.LIGHT_PURPLE
				+ "======================================");

		for (String pl : this.players.keySet()) {
			Player p = Bukkit.getPlayer(pl);

			if (this.getTerrorists().size() < this.getPolice().size()) {
				this.setTerrorist(p);
				p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD
						+ "You are a terrorist");
			} else if (this.getTerrorists().size() > this.getPolice().size()) {
				this.setPolice(p);
				p.sendMessage(ChatColor.BLUE + "" + ChatColor.BOLD
						+ "You are from the police!");
			} else {
				Integer team = new Random().nextInt(2);
				if (team == 1) {
					this.setTerrorist(p);
					p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD
							+ "You are a terrorist");
				} else {
					this.setPolice(p);
					p.sendMessage(ChatColor.BLUE + "" + ChatColor.BOLD
							+ "You are from the police!");
				}
			}
		}

		Random rldr = new Random();

		int policeldr = rldr.nextInt(this.getPoliceCount());
		String ppl = this.getPolice().toArray()[policeldr].toString();
		this.playerjobs.put(ppl, "leader");

		int terroristldr = rldr.nextInt(this.getTerroristCount());
		String tpl = this.getTerrorists().toArray()[terroristldr].toString();
		this.playerjobs.put(tpl, "leader");

		for (String pl : this.players.keySet()) {
			Player p = Bukkit.getPlayer(pl);
			if (this.isTerrorist(p)) {
				p.teleport(getRandomTerroristSpawnPoint());
			} else if (this.isPolice(p)) {
				p.teleport(getRandomPoliceSpawnPoint());
			}
			saveInv(p);
			clearInv(p);
			p.setHealth(20);
			p.setFoodLevel(20);
			p.playSound(p.getLocation(), Sound.ANVIL_LAND, 10, 1);

			p.setGameMode(org.bukkit.GameMode.ADVENTURE);

			if (isPolice(p)) {
				String ldr = this.getPoliceLeader();
				p.sendMessage(ChatColor.AQUA
						+ "Your objective is to kill the terrorist leader and then anhialate the rest of his followers.");
				if (p.getName() == ldr) {
					p.sendMessage(ChatColor.GREEN
							+ ""
							+ ChatColor.BOLD
							+ "You are the leader of the police.. If you die, your teammates won't be able to respawn!");
					p.sendMessage(ChatColor.GREEN
							+ ""
							+ ChatColor.BOLD
							+ "Your class has been changed to Leader!");
				} else {
					p.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD
							+ "Your leader is " + ldr
							+ " protect him at all cost.");
				}
			} else if (isTerrorist(p)) {
				String ldr = this.getTerroristLeader();
				p.sendMessage(ChatColor.AQUA
						+ "ATTACK! Kill the police leader and then kill the rest of his men!");
				if (p.getName() == ldr) {
					p.sendMessage(ChatColor.GREEN
							+ ""
							+ ChatColor.BOLD
							+ "You are the leader of the terrorist.. If you die, your teammates won't be able to respawn!");
					p.sendMessage(ChatColor.GREEN
							+ ""
							+ ChatColor.BOLD
							+ "Your class has been changed to Leader!");
				} else {
					p.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD
							+ "Your leader is " + ldr
							+ " protect him at all cost.");
				}
			}
		}
		for (String pl : this.getAllPlayers()) {
			Player p = Bukkit.getPlayer(pl);
			p.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD
					+ "10 seconds until the game begins.");
			if (pl != getTerroristLeader() || pl != getPoliceLeader()) //If player is not a leader
			{
				if (!playerjobs.containsKey(pl)) //If player has not selected a class
				{
					playerjobs.put(pl, "soldier"); //Set to default Class
				}
				getEquipment(pl, playerjobs.get(pl)); //Set Equipment for player 
				getBasicSuit(p); //Give Armour
				if (playerjobs.get(pl) ==null)
				{
					p.sendMessage(ChatColor.GREEN 
							+ ""
							+ ChatColor.BOLD
							+ "You are playing as a Soldier"); //Send Message confirming class
				}
				else
				{
					p.sendMessage(ChatColor.GREEN 
						+ ""
						+ ChatColor.BOLD
						+ "You are playing as a " + playerjobs.get(pl)); //Send Message confirming class
				}
			}
		}

		//Give Leader Kit
		getEquipment(getTerroristLeader(), "leader"); 
		getEquipment(getPoliceLeader(), "leader");
		
		this.mode = GameMode.INGAME;

		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				setFrozen(false);
				for (String pl : getAllPlayers()) {
					Player p = Bukkit.getPlayer(pl);
					p.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD
							+ "You are now able to move, good luck!");
				}
			}
		}, 200L);
	}

	public int getCountdownTime() {
		return this.counttime;
	}

	public String getPoliceLeader() {
		for (Entry<String, String> p : this.playerjobs.entrySet()) {
			if (p.getValue() == "leader") {
				if (isPolice(Bukkit.getPlayer(p.getKey()))) {
					return p.getKey();
				}
			}
		}
		return "";
	}

	public String getTerroristLeader() {
		for (Entry<String, String> p : this.playerjobs.entrySet()) {
			if (p.getValue() == "leader") {
				if (isTerrorist(Bukkit.getPlayer(p.getKey()))) {
					return p.getKey();
				}
			}
		}
		return "";
	}

	public Location getRandomTerroristSpawnPoint() {
		return SettingsManager.getInstance().getSpawnPoint(
				getRandomTerroristSpawn(), "terrorist");
	}

	public Location getRandomPoliceSpawnPoint() {
		return SettingsManager.getInstance().getSpawnPoint(
				getRandomTerroristSpawn(), "police");
	}


	public boolean isBlockInArena(Location v) {
		return this.arena.containsBlock(v);
	}

	public int getID() {
		return this.gameId;
	}

	public int getPoliceCount() {

		int police = 0;

		for (String p : players.values())
			if (p == "police") {
				police++;
			}
		return police;
	}

	public int getTerroristCount() {
		int terrorist = 0;

		for (String p : players.values())
			if (p == "terrorist") {
				terrorist++;
			}
		return terrorist;
	}

	public int getPlayers() {
		ArrayList<String> add = new ArrayList<String>();
		for (Entry<String, String> p : this.players.entrySet()) {
			if (p.getValue() == "terrorist" || p.getValue() == "police") {
				add.add(p.getKey());
			}
		}
		return add.size();
	}

	public ArrayList<String> getAlivePlayers() {
		ArrayList<String> add = new ArrayList<String>();
		for (Entry<String, String> p : this.players.entrySet()) {
			if (p.getValue() == "terrorist" || p.getValue() == "police") {
				add.add(p.getKey());
			}
		}
		return add;
	}

	public int getAllPlayersCount() {
		return this.players.size();
	}

	public ArrayList<String> getAllPlayers() {
		ArrayList<String> all = new ArrayList<String>();
		all.addAll(this.players.keySet());
		return all;
	}

	public ArrayList<String> getSpectators() {
		ArrayList<String> all = new ArrayList<String>();
		for (Entry<String, String> p : this.players.entrySet()) {
			if (p.getValue() == "deadterrorist" || p.getValue() == "deadpolice") {
				all.add(p.getKey());
			}
		}
		return all;
	}

	public boolean isTerrorist(Player p) {
		if (this.players.get(p.getName()) == "terrorist") {
			return true;
		}
		return false;
	}

	public boolean isPolice(Player p) {
		if (this.players.get(p.getName()) == "police") {
			return true;
		}
		return false;
	}

	public boolean isSpectating(Player p) {
		if (this.players.get(p.getName()) == "deadterrorist") {
			return true;
		}
		if (this.players.get(p.getName()) == "deadpolice") {
			return true;
		}
		return false;
	}

	public ArrayList<String> getTerrorists() {
		ArrayList<String> all = new ArrayList<String>();

		for (Entry<String, String> p : players.entrySet()) {
			if (p.getValue() == "terrorist") {
				all.add((String) p.getKey());
			}
		}
		return all;
	}

	public ArrayList<String> getPolice() {
		ArrayList<String> all = new ArrayList<String>();

		for (Entry<String, String> p : players.entrySet()) {
			if (p.getValue() == "police") {
				all.add((String) p.getKey());
			}
		}
		return all;
	}

	public boolean isInGame(Player p) {
		return this.players.keySet().contains(p.getName());
	}

	public boolean isInQueue(Player p) {
		return this.queue.contains(p);
	}

	public boolean hasPlayer(Player p) {
		return isInGame(p);
	}

	public GameMode getMode() {
		return this.mode;
	}

	public synchronized void setRBPercent(double d) {
		this.rbpercent = d;
	}

	public double getRBPercent() {
		return this.rbpercent;
	}

	public void setRBStatus(String s) {
		this.rbstatus = s;
	}

	public String getRBStatus() {
		return this.rbstatus;
	}

	public static enum GameMode {
		DISABLED, LOADING, INACTIVE, WAITING, STARTING, INGAME, FINISHING, RESETING, ERROR, SURVIVAL;
	}

	public boolean isFrozen() {
		return isfrozen;
	}

	public void setFrozen(Boolean is) {
		isfrozen = is;
	}

}
