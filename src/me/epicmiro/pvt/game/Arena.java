package me.epicmiro.pvt.game;

import org.bukkit.Location;

public class Arena
{
  Location min;
  Location max;

  public Arena(Location min, Location max)
  {
    this.max = max;
    this.min = min;
  }

  public boolean containsBlock(Location v)
  {
    if (v.getWorld() != this.min.getWorld()){
    	System.out.println("it goes wrong here");
      return false;
    }
    double x = v.getX();
    double y = v.getY();
    double z = v.getZ();

    return (x >= this.min.getBlockX()) && (x < this.max.getBlockX() + 1) && 
      (y >= this.min.getBlockY()) && (y < this.max.getBlockY() + 1) && 
      (z >= this.min.getBlockZ()) && (z < this.max.getBlockZ() + 1);
  }

  public Location getMax()
  {
    return this.max;
  }

  public Location getMin() {
    return this.min;
  }
}
