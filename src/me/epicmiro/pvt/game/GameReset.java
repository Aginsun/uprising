package me.epicmiro.pvt.game;

import me.epicmiro.pvt.settings.QueueManager;

public class GameReset {
	int gameid;
	Game g;

	public GameReset(Game g) {
		this.gameid = g.getID();
		this.g = g;
	}

	public void resetArena() {
		this.g.setRBStatus("GameReset");
		QueueManager.getInstance().rollback(this);
	}

	public void rollbackFinishedCallback() {
		this.g.resetCallback();
	}
}
