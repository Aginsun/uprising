package me.epicmiro.pvt;

import java.io.File;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Logger;

import me.epicmiro.pvt.commands.PvTCommandExecutor;
import me.epicmiro.pvt.events.BreakEvent;
import me.epicmiro.pvt.events.ChatEvent;
import me.epicmiro.pvt.events.EnderGrenade;
import me.epicmiro.pvt.events.FireBallEvent;
import me.epicmiro.pvt.events.IconMenuOpenEvent;
import me.epicmiro.pvt.events.InventoryEvent;
import me.epicmiro.pvt.events.ItemDropEvent;
import me.epicmiro.pvt.events.ItemEventHandler;
import me.epicmiro.pvt.events.JoinEvent;
import me.epicmiro.pvt.events.LeaveEvent;
import me.epicmiro.pvt.events.MoveEvent;
import me.epicmiro.pvt.events.PlayerDeathEvent;
import me.epicmiro.pvt.events.SpectatorEvent;
import me.epicmiro.pvt.events.TNTEvent;
import me.epicmiro.pvt.game.GameManager;
import me.epicmiro.pvt.settings.DatabaseManager;
import me.epicmiro.pvt.settings.ExpManager;
import me.epicmiro.pvt.settings.LoggingManager;
import me.epicmiro.pvt.settings.QueueManager;
import me.epicmiro.pvt.settings.SettingsManager;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import uk.co.aet2505.util.AutoUpdate;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;

public class PvT extends JavaPlugin {

	Logger logger;
	private static File datafolder;
	private static boolean active = false;
	public static String prefix = "[PvT]";
	public static boolean dbcon = false;
	public static HashMap<String, String> PlayerClasses = new HashMap<String, String>();
	PvT p = this;
	
	private static PvT instance = new PvT();

	public static PvT Instance()
	{
		return instance;
	}
	
	public static boolean isActive() {
		return active;
	}

	public void onEnable() {
		new AutoUpdate(this, "https://dl.dropbox.com/s/7z18ba46517zzg6/version.txt",
				"https://dl.dropbox.com/s/l1rbsawdqsu3614/Uprising.zip", false);
		SettingsManager.getInstance().setup(PvT.this.p);
		Bukkit.getServer().getScheduler()
				.scheduleSyncDelayedTask(this, new Startup(), 10L);
	}

	public void onDisable() {
		PluginDescriptionFile pdfFile = this.p.getDescription();
		SettingsManager.getInstance().saveSpawns();
		SettingsManager.getInstance().saveSystemConfig();	
		GameManager.getInstance().getGame().disable();
		Bukkit.getScheduler().cancelTasks(p);
	}

	public WorldEditPlugin getWorldEdit() {
		Plugin worldEdit = getServer().getPluginManager()
				.getPlugin("WorldEdit");
		if ((worldEdit instanceof WorldEditPlugin)) {
			return (WorldEditPlugin) worldEdit;
		}
		return null;
	}

	public void setCommands() {
		getCommand("pvt").setExecutor(new PvTCommandExecutor(this.p));
	}

	public void loadConfigs() {
		SettingsManager.getInstance().setup(PvT.this.p);
		GameManager.getInstance().setup(PvT.this.p);
		DatabaseManager.getInstance().setup(PvT.this.p);
		try {
			QueueManager.getInstance().setup(PvT.this.p, true);
			ExpManager.getInstance().setup(PvT.this.p, true);
			PvT.dbcon = true;
		} catch (SQLException e) {
			PvT.dbcon = false;
			e.printStackTrace();
			PvT.this.logger.severe("Failed to connect to the database. Please check the settings and try again!!!");
		}
	}

	public void registerEvents() {

		PluginManager pm = PvT.this.getServer().getPluginManager();

		pm.registerEvents(new JoinEvent(PvT.this.p), PvT.this.p);
		pm.registerEvents(new LeaveEvent(PvT.this.p), PvT.this.p);
		pm.registerEvents(new PlayerDeathEvent(PvT.this.p), PvT.this.p);
		pm.registerEvents(new InventoryEvent(PvT.this.p), PvT.this.p);
		pm.registerEvents(new SpectatorEvent(PvT.this.p), PvT.this.p);
		pm.registerEvents(new ChatEvent(PvT.this.p), PvT.this.p);
		pm.registerEvents(new EnderGrenade(PvT.this.p), PvT.this.p);
		pm.registerEvents(new BreakEvent(PvT.this.p), PvT.this.p);
		pm.registerEvents(new TNTEvent(PvT.this.p), PvT.this.p);
		pm.registerEvents(new FireBallEvent(PvT.this.p), PvT.this.p);
		pm.registerEvents(new MoveEvent(PvT.this.p), PvT.this.p);
		pm.registerEvents(new ItemEventHandler(PvT.this.p), PvT.this.p);
		pm.registerEvents(new IconMenuOpenEvent(this.p), this.p);
		pm.registerEvents(new ItemDropEvent(p), p);
		pm.registerEvents(LoggingManager.getInstance(), PvT.this.p);

	}
	
	public void addClassesToList(Player p, String string)
	{
		PlayerClasses.put(p.getName(), string);
	}
	
	public boolean doesPlayerOwnClass(Player p, String s)
	{
		if(PlayerClasses.containsKey(p.getName()))
		{
			String classes = PlayerClasses.get(p.getName());
			String[] classList = classes.split(";");
			for(String string : classList)
			{
				System.out.println(string);
			}
			if(s.equals("medic"))
				if(classList[1].equals("1"))
					return true;
			if(s.equals("sniper"))
				if(classList[2].equals("1"))
					return true;
			if(s.equals("pyro"))
				if(classList[3].equals("1"))
					return true;
			if(s.equals("bomber"))
				if(classList[4].equals("1"))
					return true;
		}
		return false;
	}

	class Startup implements Runnable {
		Startup() {
		}

		public void run() {
			PvT.this.logger = PvT.this.p.getLogger();
			PvT.active = true;
			PvT.datafolder = PvT.this.p.getDataFolder();

			PvT.this.loadConfigs();
			PvT.this.setCommands();
			PvT.this.registerEvents();

			for (Player p : Bukkit.getOnlinePlayers())
				if(GameManager.getInstance().getGame() != null){
				if (GameManager.getInstance().getBlockGameId(p.getLocation()) != -1){
					if(SettingsManager.getInstance().getLobbySpawn() != null){
					p.teleport(SettingsManager.getInstance().getLobbySpawn());
					}
				}
				}
		}
	}
}
