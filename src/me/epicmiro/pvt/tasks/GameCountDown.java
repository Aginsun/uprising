package me.epicmiro.pvt.tasks;

import me.epicmiro.pvt.game.GameManager;
import me.epicmiro.pvt.settings.SettingsManager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class GameCountDown extends BukkitRunnable {

	public int remainingtime;
	private final JavaPlugin plugin;
	private int taskId;

	public GameCountDown(JavaPlugin plugin) {
		this.plugin = plugin;
	}

	public void start() {
		this.remainingtime = 60;
		this.taskId = plugin.getServer().getScheduler()
				.scheduleSyncRepeatingTask(plugin, this, 0L, 200L);
	}

	public void run() {
		if (Bukkit.getOnlinePlayers().length == 0) {
			//do nothing
		} else {
			remainingtime = remainingtime - 10;
			if (remainingtime <= 0) {
				if (Bukkit.getOnlinePlayers().length >= SettingsManager
						.getInstance().getAutoStart()) {
					for (Player p : Bukkit.getOnlinePlayers()) {
						GameManager.getInstance().addPlayer(p);
					}
					GameManager.getInstance().getGame().startGame();
					Bukkit.getScheduler().cancelTask(this.taskId);
				} else {
					remainingtime = 30;
					Bukkit.broadcastMessage(ChatColor.LIGHT_PURPLE
							+ "======================================");
					Bukkit.broadcastMessage(ChatColor.LIGHT_PURPLE + ""
							+ ChatColor.BOLD + "There were not enough players.");
					Bukkit.broadcastMessage(ChatColor.LIGHT_PURPLE + ""
							+ ChatColor.BOLD + "Retrying in " + remainingtime
							+ " second(s).");
					Bukkit.broadcastMessage(ChatColor.LIGHT_PURPLE
							+ "======================================");
				}
			} else {
				Bukkit.broadcastMessage(ChatColor.LIGHT_PURPLE
						+ "======================================");
				Bukkit.broadcastMessage(ChatColor.LIGHT_PURPLE + ""
						+ ChatColor.BOLD + "Game starting in " + remainingtime
						+ " second(s).");
				Bukkit.broadcastMessage(ChatColor.LIGHT_PURPLE
						+ "======================================");
			}
		}
	}
}
