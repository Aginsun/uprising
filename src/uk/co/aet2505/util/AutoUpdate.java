package uk.co.aet2505.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.SimplePluginManager;
import org.bukkit.scheduler.BukkitScheduler;

public class AutoUpdate implements Listener, CommandExecutor{
	
/**
 * NOTE: This Class and all classes, interfaces and other documents in the 'uk.co.aet2505.util' package have been developed by Adam Thomas (from this point will be refereed to as me)
 * and ARE NOT exclusively part of any project in which they are included. The owner of this class and its contents is me despite the nature of the
 * project it is included in.
 * 
 * To discuss the usage Terms of this package and its contents please feel free to contact me. Full contact information can be found in the Terms.txt as well as a copy of this notice file included in this package.
 *
 * @author adam
 */
	
	private String versionURL = "";
	private String updateFileURL = "";
	private boolean debug = false;
	private Plugin plugin;
	private final AtomicBoolean lock = new AtomicBoolean(false);
	private boolean needUpdate = false;
	private boolean updatePending = false;
	private ArrayList<String> devs = new ArrayList<String>();
	
	/**
	 * Initialises the Auto Updater. Please configure the Update Urls and Debug mode in this class
	 * 
	 * @param instance
	 * 		Main instance of the Plugin (The one that extends JavaPlugin)
	 */
	private AutoUpdate(Plugin instance)
	{
		plugin = instance;
		init();
	}
	
	/**
	 * Constructor for the AutoUpdate Class
	 * 
	 * @param instance
	 * 		Main instance of the Plugin (The one that extends JavaPlugin)
	 * @param versionURL
	 * 		The Url to the version.txt file
	 * @param updateFileURL
	 * 		The url to the Zip file containing the jar file.
	 */
	public AutoUpdate(Plugin instance, String versionURL, String updateFileURL)
	{
		this(instance, versionURL, updateFileURL, false);
	}
	
	/**
	 * Constructor for the AutoUpdate Class
	 * 
	 * @param instance
	 * 		Main instance of the Plugin (The one that extends JavaPlugin)
	 * @param versionURL
	 * 		The Url to the version.txt file
	 * @param updateFileURL
	 * 		The url to the Zip file containing the jar file.
	 * @param debug
	 * 		If Debug mode is enabled.
	 */
	public AutoUpdate(Plugin instance, String versionURL, String updateFileURL, boolean debug)
	{
		this.debug = debug;
		this.updateFileURL = updateFileURL;
		this.versionURL = versionURL;
		plugin = instance;
		init();
	}
	
	private void init()
	{
		registerCommand();
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		
		devs.add("aet2505");
		
		plugin.getLogger().info("Updater initialised. Checking for updates");
		versionCheck();
	}
	
	/**
	 * Checks the local version to the remote Version.
	 * 
	 * @return
	 * 		True: New version found
	 * 		False: No New Version
	 */
	private boolean versionCheck()
	{
		String currentVersionStr = plugin.getDescription().getVersion();
		String remoteVersionStr = getRemoteVersion(); // Get the newest file's version number
		
		int currentVersion = convertVersionToInt(currentVersionStr);
		int remoteVersion = convertVersionToInt(remoteVersionStr);
		
		if (remoteVersion > currentVersion)
		{
			needUpdate = true;
			plugin.getLogger().info("Update found for " + plugin.getDescription().getName());
			plugin.getLogger().info("Use /update" + plugin.getDescription().getName() + "  to start an update");
			
			for (Player player: Bukkit.getOnlinePlayers())
			{
				if (player.isOp() || devs.contains(player.getName()))
				{
					player.sendMessage(ChatColor.GOLD + "Update found for " + plugin.getDescription().getName());
					player.sendMessage(ChatColor.GOLD + "Use /update" + plugin.getDescription().getName() + " to start an update");
				}
			}
			return true;
		}
		else
		{
			plugin.getLogger().info("No update found");
			return false;
		}
	}
	
	/**
	 * Converts the version sent to it from a string to an integer.
	 * <p>
	 * For Example: 1.2.1 goes to 121
	 * 
	 * @param ver
	 * 		The version String
	 * @return
	 * 		The version int
	 */
	private int convertVersionToInt(String ver)
	{
        if(ver.contains("."))
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i <ver.length(); i++)
            {
                Character c = ver.charAt(i);
                if (Character.isLetterOrDigit(c))
                {
                    sb.append(c);
                }
            }
            return Integer.parseInt(sb.toString());
        }
        return Integer.parseInt(ver);
	}
	
	/**
	 * Gets the remote version from the version file at the remote URL
	 * 
	 * @return
	 * 		Returns the contents of the version file at the specified URL
	 */
	private String getRemoteVersion()
	{
		try {
			URL url = new URL(versionURL);
			InputStream is;
			is = url.openStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String version = reader.readLine();
			if (debug) plugin.getLogger().info(version);
			return version;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Downloads and prepares the New version for install.
	 * 
	 * @param sender
	 * 		The sender of the command.
	 */
	@SuppressWarnings("deprecation")
	private void update(CommandSender sender)
	{
		if (debug) plugin.getLogger().info("Downloading Update for " + plugin.getDescription().getName());
		
		final BukkitScheduler bs = plugin.getServer().getScheduler();
		final String pn = sender instanceof Player ? ((Player)sender).getName() : null;
		bs.scheduleAsyncDelayedTask(plugin, new Runnable()
		{
			public void run()
			{
				try
				{
					while(!lock.compareAndSet(false, true))
					{
						try
						{
							Thread.sleep(1L);
						}
						catch(InterruptedException e)
						{
						}
					}
					String out;
					try
					{
						File to = new File(plugin.getServer().getUpdateFolderFile(), plugin.getDescription().getName() + ".zip");
						File tmp = new File(to.getPath()+".au");
						if(!tmp.exists())
						{
							plugin.getServer().getUpdateFolderFile().mkdirs();
							tmp.createNewFile();
						}
						URL url = new URL(updateFileURL);
						InputStream is = url.openStream();
						OutputStream os = new FileOutputStream(tmp);
						byte[] buffer = new byte[4096];
						int fetched;
						while((fetched = is.read(buffer)) != -1)
							os.write(buffer, 0, fetched);
						is.close();
						os.flush();
						os.close();
						if(to.exists())
							to.delete();
						tmp.renameTo(to);
						unzip(to);
						tmp.delete();
						if (debug) plugin.getLogger().info("Download Complete for " + plugin.getDescription().getName());
					}
					catch(Exception e)
					{
						plugin.getLogger().warning("Failed to update " + plugin.getDescription().getName());
					}
					lock.set(false);
				}
				catch(Throwable t)
				{}
			}
		});
	}
	

	private void unzip(File zip) {
        try {   
        	InputStream in = new BufferedInputStream(new FileInputStream(zip)); 
	
        	ZipInputStream zin = new ZipInputStream(in);  
            ZipEntry e;  
            
            while((e=zin.getNextEntry())!= null) {                  
                String s = e.getName();  
                File f = new File(plugin.getServer().getUpdateFolderFile(), s);  
                plugin.getLogger().info("unzipping " + s);
                FileOutputStream out = new FileOutputStream(f);  
                byte [] b = new byte[512];  
                int len = 0;  
                while ( (len=zin.read(b))!= -1 ) {  
                    out.write(b,0,len);  
                }  
                out.close();                                      
            }  
            zin.close();  
            zip.delete();
        } catch (IOException e) {}
	}
	
	/**
	 * Registers the "/update" + plugin.getDescription().getName() command for use.
	 * This command does NOT need to be added in plugin.yml
	 */
	  private void registerCommand()
	  {
		try
		{
			SimplePluginManager pm = (SimplePluginManager)plugin.getServer().getPluginManager();
			Field f = SimplePluginManager.class.getDeclaredField("commandMap");
			f.setAccessible(true);
			SimpleCommandMap cm = (SimpleCommandMap)f.get(pm);
			f.setAccessible(false);
			if(cm.getCommand("update" + plugin.getDescription().getName()) == null) // First!
			{
				Constructor<PluginCommand> c = PluginCommand.class.getDeclaredConstructor(String.class, Plugin.class);
				c.setAccessible(true);
				PluginCommand cmd = c.newInstance("update", plugin);
				c.setAccessible(false);
				cmd.setExecutor(this);
				cm.register("update", cmd);
			}
		}
		catch(Throwable t) {}
	  }

	/**
	 * Handles the "/update" + plugin.getDescription().getName() command.
	 * 
	 * Do not call remotely
	 */
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label,
			String[] args) {
		if (sender.isOp() || devs.contains(sender.getName()))
		{
			if (needUpdate)
			{
				update(sender);
				updatePending = true;
				needUpdate = false;
				for (Player player: Bukkit.getOnlinePlayers())
				{
					if (player.isOp() || devs.contains(player.getName()))
					{
						player.sendMessage(ChatColor.GOLD + "Please reload or restart to apply the update for " + plugin.getDescription().getName());
					}
				}
			}
			else 
			{
				sender.sendMessage(ChatColor.RED + "No Update needed.");
			}
			return true;
		}
		else
		{
			sender.sendMessage(ChatColor.RED + "I'm sorry but you do not have permission to perform updates");
			return true;
		}
	}
	
	/**
	 * Handles Player Join Event
	 * 
	 * Do not call remotely.
	 */
	@EventHandler
	public void onPlayerJoin(PlayerLoginEvent evt)
	{
		Player player = evt.getPlayer();
		if (needUpdate)
		{
			if (player.isOp() || devs.contains(player.getName()))
			{
				player.sendMessage(ChatColor.GOLD + "Update found for " + plugin.getDescription().getName());
				player.sendMessage(ChatColor.GOLD + "Use /update" + plugin.getDescription().getName() + " to start an update");
			}
		}
		else if (updatePending)
		{
			if (player.isOp() || devs.contains(player.getName()))
			{
				player.sendMessage(ChatColor.GOLD + "Please reload or restart to apply the update for " + plugin.getDescription().getName());
			}
		}
	}
}
